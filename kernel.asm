bits 32
section .text
;; Multiboot
align 4
dd 0x1BADB002
dd 0x00
dd - (0x1BADB002 + 0x00)
;;

global start
global read_port
global read_port_word
global write_port
global write_port_word
global idt_load
global gdt_flush
global tss_flush
global read_eip

extern irq_handle
extern isr_handle
extern _IDTptr
extern _GDTptr

extern kstart

read_port:
    mov edx, [esp + 4]
    in al, dx
    ret

read_port_word:
    mov edx, [esp + 4]
    in ax, dx
    ret

write_port:
    mov edx, [esp + 4]    
    mov al, [esp + 4 + 4]  
    out dx, al  
    ret

write_port_word:
    mov edx, [esp + 4]
    mov ax, [esp + 4 + 4]
    out dx, ax
    ret

idt_load:
    lidt [_IDTptr]
    ret

tss_flush:
    mov ax, 0x2B
    ltr ax
    ret

gdt_flush:
    lgdt [_GDTptr]
    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax
    jmp 0x08:gflush2
gflush2:
    ret

read_eip:
    pop eax
    jmp eax

%macro IRQ_ENTRY 2
    global asm_irq%1
    asm_irq%1:
        cli
        push byte 0
        push byte %2
        jmp irq_common_stub
%endmacro

%macro ISR_NOERR 1
    global asm_isr%1
    asm_isr%1:
        cli
        push byte 0
        push byte %1
        jmp isr_common_stub
%endmacro

%macro ISR_ERR 1
    global asm_isr%1
    asm_isr%1:
        cli
        push byte %1
        jmp isr_common_stub
%endmacro

isr_common_stub:
    pusha
    push ds
    push es
    push fs
    push gs
    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov eax, esp
    push eax
    mov eax, isr_handle
    call eax
    pop eax
    pop gs
    pop fs
    pop es
    pop ds
    popa
    add esp, 8
    iret

ISR_NOERR 0
ISR_NOERR 1
ISR_NOERR 2
ISR_NOERR 3
ISR_NOERR 4
ISR_NOERR 5
ISR_NOERR 6
ISR_NOERR 7
ISR_ERR   8
ISR_NOERR 9
ISR_ERR   10
ISR_ERR   11
ISR_ERR   12
ISR_ERR   13
ISR_ERR   14
ISR_NOERR 15
ISR_NOERR 16
ISR_NOERR 17
ISR_NOERR 18
ISR_NOERR 19
ISR_NOERR 20
ISR_NOERR 21
ISR_NOERR 22
ISR_NOERR 23
ISR_NOERR 24
ISR_NOERR 25
ISR_NOERR 26
ISR_NOERR 27
ISR_NOERR 28
ISR_NOERR 29
ISR_NOERR 30
ISR_NOERR 31
ISR_NOERR 127

irq_common_stub:
    pusha
    push ds
    push es
    push fs
    push gs
    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov eax, esp
    push eax
    ; Call the C kernel hardware interrupt handler
    mov eax, irq_handle
    call eax
    pop eax
    pop gs
    pop fs
    pop es
    pop ds
    popa
    add esp, 8
    iret

IRQ_ENTRY 0, 0x20
IRQ_ENTRY 1, 0x21
IRQ_ENTRY 2, 0x22
IRQ_ENTRY 3, 0x23
IRQ_ENTRY 4, 0x24
IRQ_ENTRY 5, 0x25
IRQ_ENTRY 6, 0x26
IRQ_ENTRY 7, 0x27
IRQ_ENTRY 8, 0x28
IRQ_ENTRY 9, 0x29
IRQ_ENTRY 10, 0x2A
IRQ_ENTRY 11, 0x2B
IRQ_ENTRY 12, 0x2C
IRQ_ENTRY 13, 0x2D
IRQ_ENTRY 14, 0x2E
IRQ_ENTRY 15, 0x2F

start:
    cli
    mov esp, stack_space
    push esp

    ;; multiboot
    push eax
    push ebx
    ;;
    call kstart
    hlt

section .bss
resb 8192          ;; 8k stack
stack_space:
