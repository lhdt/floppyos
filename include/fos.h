#ifndef FOS_H
#define FOS_H

#define KERNEL_NAME "Floppyk"
#define KERNEL_VER "0.0.1"

#define KERNEL_ARCH "i686"

#include "stdint.h"

typedef uint32_t mboot_uint;

// WARNING! DON'T CHANGE THIS STRUCT
struct multiboot_s
{
    mboot_uint flags;
    mboot_uint mem_low;
    mboot_uint mem_high;
    mboot_uint boot_device;
    mboot_uint cmdline;
    mboot_uint mod_count;
    mboot_uint mod_addr;
    mboot_uint num;
    mboot_uint size;
    mboot_uint addr;
    mboot_uint shndx;
    mboot_uint mmap_len;
    mboot_uint mmap_addr;
    mboot_uint drives_len;
    mboot_uint drives_addr;
    mboot_uint conf_table;
    mboot_uint bootloader_name;
    mboot_uint apm_table;
    mboot_uint vbe_cinfo; // control info
    mboot_uint vbe_minfo; // mode info
    mboot_uint vbe_mode;
    mboot_uint vbe_if_seg; // interface segment
    mboot_uint vbe_if_off;
    mboot_uint vbe_if_len;
} __attribute__((packed));
typedef struct multiboot_s* multiboot_ptr;
//


// WARNING! DON'T CHANGE THIS STRUCT
/*
 * GDT entry structure
 * Source: https://wiki.osdev.org/Getting_to_Ring_3
*/
struct gdt_entry_s
{
    uint16_t limit_low;
    uint16_t base_low;
    uint8_t base_mid;
    uint8_t access;
    uint8_t gran;
    uint8_t base_high;
}__attribute__((packed));
typedef struct gdt_entry_s gdt_entry_t;

struct gdt_ptr_s
{
    uint16_t limit;
    uint32_t base;
}__attribute__((packed));
typedef struct gdt_ptr_s gdt_ptr_t;
//

// WARNING! DON'T CHANGE THIS STRUCT
/*
 * GDT entry structure
 * Source: https://wiki.osdev.org/Getting_to_Ring_3
*/
struct tss_entry_s
{
    uint32_t prev_tss;
    uint32_t esp0;
    uint32_t ss0;
    uint32_t esp1;
    uint32_t ss1;
    uint32_t esp2;
    uint32_t ss2;
    uint32_t cr3;
    uint32_t eip;
    uint32_t eflags;
    uint32_t eax;
    uint32_t ecx;
    uint32_t edx;
    uint32_t ebx;
    uint32_t esp;
    uint32_t ebp;
    uint32_t esi;
    uint32_t edi;
    uint32_t es;
    uint32_t cs;
    uint32_t ss;
    uint32_t ds;
    uint32_t fs;
    uint32_t gs;
    uint32_t ldt;
    uint16_t trap;
    uint16_t iomap_base;
}__attribute__((packed));
typedef struct tss_entry_s tss_entry_t;
//

//
#define FOS_READONLY
//

#endif // FOS_H
