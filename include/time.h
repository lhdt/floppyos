#ifndef TIME_H
#define TIME_H

#include "stdint.h"
#include "memory.h"

uint8_t get_secs(void);
uint8_t get_mins(void);
uint8_t get_hours(void);
cstring get_time_cstr(void);
uint32_t get_timesum(void);

#endif // TIME_H
