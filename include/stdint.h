#ifndef STDINT_H
#define STDINT_H

#define NULL ((void*)0)

typedef signed char int8_t;
typedef unsigned char uint8_t;

typedef signed short int16_t;
typedef unsigned short uint16_t;

typedef signed int int32_t;
typedef unsigned int uint32_t;
typedef uint32_t size_t; // address type

typedef signed long long int64_t;
typedef unsigned long long uint64_t;

typedef char* cstring;

struct string_s
{
    cstring data;
    int size;
}__attribute__((packed));
typedef struct string_s string;

typedef uint8_t bool;
#define true 1
#define false 0

#endif // STDINT_H
