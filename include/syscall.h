#ifndef SYSCALL_H
#define SYSCALL_H

#include "stdint.h"
#include "lla.h"
#include "drivers/isr.h"

enum SYSCALLS
{
    CALL_PUTC=1,
    CALL_PUTI=2,
    CALL_PUTH=4,
    CALL_PUTS=8,
};

void syscall_init(void);

void syscall(struct regs_s *r);

#endif // SYSCALL_H
