#ifndef STDIO_H
#define STDIO_H

#include "stdint.h"

#define SYSCALL __asm__ __volatile__("int $0x7F")

void puts(cstring s);
void puti(int a);
void puth(uint32_t a);

#endif // STDIO_H
