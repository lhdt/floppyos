#ifndef STDLIB_H
#define STDLIB_H

#include "stdint.h"
#include "memory.h"

#define MAX_STRLEN (1024*1024)

// Random
void srand(uint32_t next);
uint32_t rand(void);
//

// String operations
int strlen(const cstring s);

int _strcmp(const cstring s1, const cstring s2, bool cis, int len);
int strcmp(const cstring s1, const cstring s2);
int strcmpp(const cstring s1, const cstring s2, int len);
int strcmpi(const cstring s1, const cstring s2); // case-insensitive
int strncmpi(const cstring s1, const cstring s2, int len); // case-insensitive, length-restricted

bool streqi(const string *s1, const cstring s2);
bool streq(const string *s1, const cstring s2);
bool strneqi(const string *s1, const cstring s2, int len);

void strcpy(const cstring src, cstring dst);
void strncpy(const cstring src, cstring dst, int len);

void substr(const cstring src, int start, int len, cstring out);

void strrev(cstring s, int len);

cstring itoa(uint32_t a, int len, int base, char fill);
int atoi(const cstring s, int l, int base);
//

#endif // STDLIB_H
