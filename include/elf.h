#ifndef ELF_H
#define ELF_H

#include "stdint.h"

#define ELF_SIGN ((uint32_t)0x464C457F)

enum ELF_MACHINES
{
    EM_NONE=0x00,
    EM_386=0x03,
    EM_PPC=0x14,
    EM_PPC64=0x15,
    EM_X86_64=0x3E,
};

enum ELF_FTYPES
{
    ET_NONE=0x00,
    ET_REL=0x01,
    ET_EXEC=0x02,
    ET_DYN=0x03,
    ET_CORE=0x04,
};

struct elf32_header_s
{
    char e_ident[16]; // identification
    uint16_t e_type;
    uint16_t e_machine;
    uint32_t e_version; // must be 1 for correctness
    uint32_t e_entry; // entry point (should exist?)
    uint32_t e_phoff; // program header table (if exists)
    uint32_t e_shoff; // section header table (if exists)
    uint32_t e_flags;
    uint16_t e_ehsize;
    uint16_t e_phentsize;
    uint16_t e_phnum;
    uint16_t e_shentsize;
    uint16_t e_shnum;
    uint16_t e_shstrndx;
}__attribute__((packed));
typedef struct elf32_header_s elf32_header_t;

void elf_parse(const void *data, size_t size, uint32_t *entry);

#endif // ELF_H
