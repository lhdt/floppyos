#ifndef LLA_H
#define LLA_H

#include "stdint.h"

#define INTEL_MCODE (uint32_t)(0x756e6547)
#define AMD_MCODE (uint32_t)(0x68747541)

#define cpuid(in, a, b, c, d) __asm__ __volatile__("cpuid": "=a" (a), "=b" (b), "=c" (c), "=d" (d) : "a" (in))

/*
 * Source: https://github.com/descent/osdev/blob/master/kernel/sys/task.c
*/
// ---------------------------------------------------------------------------------------------
#define PUSH(stack, type, item) stack -= sizeof(type); \
                                *((type *) stack) = item
// ---------------------------------------------------------------------------------------------

struct regs_s {
    uint32_t gs, fs, es, ds;
    uint32_t edi, esi, ebp, esp, ebx, edx, ecx, eax;
    uint32_t int_id, err_code;
    uint32_t eip, cs, eflags, useresp, ss;
}__attribute__((packed));


static inline void outl(uint16_t port, uint32_t v)
{
    __asm__ __volatile__("outl %0, %1" : : "a"(v), "d"(port));
}

static inline uint32_t inl(uint16_t port)
{
    uint32_t ret;
    __asm__ __volatile__("inl %1, %0" : "=a"(ret) : "d"(port));
    return ret;
}

static inline void set_eax(uint32_t a)
{
    __asm__ __volatile__("mov %0, %%eax" : : "a"(a):);
}

static inline void set_ebx(uint32_t a)
{
    __asm__ __volatile__("mov %0, %%ebx" : : "a"(a):);
}

static inline void set_edi(uint32_t a)
{
    __asm__ __volatile__("mov %0, %%edi" : : "a"(a):);
}

static inline void set_esi(uint32_t a)
{
    __asm__ __volatile__("mov %0, %%esi" : : "a"(a):);
}

#endif // LLA_H
