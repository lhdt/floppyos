#ifndef MATH_H
#define MATH_H

#include "stdint.h"

#define M_PI  (3.14159265358979323846)
#define M_PIf (3.1415927f)

#define DEG_IN_RADf (57.2958f)
#define DEG_IN_RAD  (57.2958)

#define RAD_IN_DEGf (0.0174533f)
#define RAD_IN_DEG  (0.0174533)

float radf(float _deg);
float degf(float _rad);

float sinf(float a);
float cosf(float a);

float absf(float a);
int abs(int a);

int fac(int a);

float powf(float x, int a);
int powi(int x, int a);

#endif // MATH_H
