#ifndef MEMORY_H
#define MEMORY_H

#include "stdint.h"

// 14 MiB memory
#define BASE_ADDR (size_t)(0x01000000)
// Should not permit using above this point
#define MAX_ADDR  (size_t)(0x01FFFFFF)

#define VALIDADDR(a) (((a)>=BASE_ADDR) && ((a)<MAX_ADDR))

typedef void* generic;

cstring new_cstr(int len);
void resize_cstr(cstring s, int o_len, int n_len);
void del_cstr(cstring s, int len);

string *new_str(int len);
void del_str(string *s);

string *create_str(const cstring src);

cstring *new_cstr_array(int s_len, int a_len);
void del_cstr_array(cstring *sa, int s_len, int a_len);

void memset(generic p, char c, int size);
void memcpy(const generic src, generic dst, int size);

uint32_t memload(void);

char *last_ptr(void);

#endif // MEMORY_H
