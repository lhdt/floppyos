#ifndef HCORE_H
#define HCORE_H

#include "stdint.h"
#include "memory.h"

struct hWindow_s
{
    uint16_t x, y;
    uint16_t w, h;
    uint8_t wid;

    string *title;

    uint8_t zorder;
    uint8_t mCol, aCol;
} __attribute__((packed));
typedef struct hWindow_s *hWindow;

enum HW_DRAWBUFF_FUNCS
{
    DRAWBUFF_NONE=0x00,
    DRAWBUFF_XOR=0x03,
    DRAWBUFF_OR=0x01,
    DRAWBUFF_AND=0x04,
};

enum HW_WTYPES
{
    HW_NONE=0,
    HW_WINDOW=7,
};

void *hw_new_widget(uint8_t wtype);
void hw_del_widget(void *widget);

void hw_drawbuff_init(uint16_t w, uint16_t h);

void hw_set_size(void *widget, uint16_t w, uint16_t h);
void hw_set_pos(void *widget, uint16_t x, uint16_t y);

void hw_draw_widget(void *widget);

#endif // HCORE_H
