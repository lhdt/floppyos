#ifndef IDT_H
#define IDT_H

#include "stdint.h"

struct idt_entry_s
{
    uint16_t base_low;
    uint16_t selector;
    uint8_t zero;
    uint8_t flags;
    uint16_t base_high;
}__attribute__((packed));
typedef struct idt_entry_s idt_entry_t;

struct idt_ptr_s
{
    uint16_t limit;
    uint32_t base;
}__attribute__((packed));
typedef struct idt_ptr_s idt_ptr_t;

void idt_init(void);

void idt_set_gate(uint8_t idt_id, uint32_t base, uint16_t selector, uint8_t flags);

extern idt_ptr_t _IDTptr;

#endif // IDT_H
