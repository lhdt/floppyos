#ifndef ATA_H
#define ATA_H

#include "stdlib.h"

#define ATA_BLOCKSIZE 512

#define ATA_PRIMARY_BASE_PORT (0x1f0)
#define ATA_SECONDARY_BASE_PORT (0x170)

// Handy messages
#define MSG_ATA_DISKNFOUND(d) "Disk " d " not found or can't be accessed\n"
//

enum LBA_CMDS
{
    LBA28_READR=0x20, // read with retry
    LBA28_WRITER=0x30, // write with retry
    LBA28_IDENTIFY=0xEC,
    LBA28_CACHEFLUSH=0xE7,
};

struct ATA_info_s
{
    char type[10]; // HDD or None
    uint32_t blocks;
    uint8_t cgrade; // uint64_t fullcapacity = ((uint64_t)capacity << (10*cgrade))
    bool connected;
}__attribute__((packed));
typedef struct ATA_info_s ATA_info;

int hdd_writeblk(bool primary, uint8_t drive, uint32_t baddr, const cstring data);
int hdd_readblk(bool primary, uint8_t drive, uint32_t baddr, cstring data);

int hdd_format(bool primary, uint8_t drive);

int hdd_sync(void);

ATA_info *hdd_infostructptr(void);

bool hdd_ready(bool primary);
bool hdd_status(bool primary);
bool hdd_exists(bool primary, uint8_t driveid);

int hdd_info(bool primary, uint8_t drive, uint32_t *blockcount);

#endif // ATA_H
