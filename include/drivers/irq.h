#ifndef IRQ_H
#define IRQ_H

#include "stdint.h"
#include "lla.h"

#define IDT_SIZE 16

typedef void (*irq_handler_t)(struct regs_s*);

#define KEYBD_IRQ 0x1
#define PSM_IRQ 0xC

#define IRQNO(a) (uint32_t)(asm_irq ## a)

void irq_install_handler(uint8_t irq_id, irq_handler_t handle);
void irq_remove_handler(uint8_t irq_id);

void irq_map(void);
void irq_init(void);
void irq_gates(void);
void irq_ack(uint8_t irq_id); // acknowledge

void irq_handle(struct regs_s *r);

#endif // IRQ_H
