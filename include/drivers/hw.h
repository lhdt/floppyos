#ifndef HW_H
#define HW_H

#include "stdint.h"
#include "fos.h"

void gdt_init(void);
void gdt_set_gate(uint8_t gdt_id, uint32_t base, uint32_t limit, uint8_t access, uint8_t gran);

void tss_init(gdt_entry_t *g);
void tss_write(uint32_t tss_id, uint16_t ss0, uint32_t esp0);
void set_kernel_stack(uint32_t stack);

extern gdt_ptr_t _GDTptr;

#endif // HW_H
