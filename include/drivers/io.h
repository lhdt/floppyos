#ifndef IO_H
#define IO_H

#include "stdint.h"
#include "stdlib.h"

void kputs(cstring s);
void kputstr(string *s);
void kputsn(cstring s, int len);

void kputi(int a);
void kputh(uint32_t a);
void kputc(char c);
void kputcc(char c, uint8_t col);

char kgetc(void);
uint8_t kgetk(void);
void kgets(string *buffstr, bool echo);

void kclear(void);
void kclearline(uint16_t line);
uint16_t kgetline(void);

void kscroll(void);
void kchkcur(void);
void knewline(void);
void kupdcur(void);

void ksetcol(uint8_t col);
uint8_t kgetcol(void);

#endif // IO_H
