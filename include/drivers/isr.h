#ifndef ISR_H
#define ISR_H

#include "drivers/idt.h"
#include "drivers/irq.h"

#define ISR_COUNT (32)

#define ISRNO(a) (uint32_t)(asm_isr ## a)

void isr_init(void);

void isr_install_handler(uint8_t isr_id, irq_handler_t handler);
void isr_remove_handler(uint8_t isr_id);
void isr_handle(struct regs_s *r);

cstring isr_msg(uint8_t err);

#endif // ISR_H
