#ifndef PSM_H
#define PSM_H

#include "stdint.h"
#include "drivers/irq.h"

#define PSM_DATA (0x60)
#define PSM_STATUS (0x64)

void psm_init(void);
void psm_wait(uint8_t a_t);
void psm_handler(struct regs_s *r);

void psm_write(uint8_t v);
uint8_t psm_read(void);

void psm_get_pos(uint16_t *outx, uint16_t *outy);

#endif // PSM_H
