#ifndef VGA_H
#define VGA_H

#include "stdint.h"

enum VGA_PORTS
{
    VGA_MISC=0x3c2,
    VGA_CRTC_IDX=0x3d4,
    VGA_CRTC_DATA=0x3d5,
    VGA_SEQ_IDX=0x3c4,
    VGA_SEQ_DATA=0x3c5,
    VGA_GCON_IDX=0x3ce,
    VGA_GCON_DATA=0x3cf,
    VGA_ATTR_IDX=0x3c0,
    VGA_ATTR_READ=0x3c1,
    VGA_ATTR_WRITE=0x3c0,
    VGA_ATTR_RESET=0x3da,
};

enum VGA_COLORS_16k
{
    VGA_COLOR_BLACK=0x00,
    VGA_COLOR_BLUE=0x01,
    VGA_COLOR_GREEN=0x02,
    VGA_COLOR_CYAN=0x03,
    VGA_COLOR_RED=0x04,
    VGA_COLOR_MAGENTA=0x05,
    VGA_COLOR_BROWN=0x06,
    VGA_COLOR_WHITE=0x07,
    VGA_COLOR_GRAY=0x08,
    VGA_COLOR_LIGHT_BLUE=0x09,
    VGA_COLOR_LIGHT_GREEN=0x0A,
    VGA_COLOR_LIGHT_CYAN=0x0B,
    VGA_COLOR_LIGHT_RED=0x0C,
    VGA_COLOR_LIGHT_MAGENTA=0x0D,
    VGA_COLOR_YELLOW=0x0E,
    VGA_COLOR_LIGHT_WHITE=0x0F,
};

bool vga_is_mode_supported(uint16_t width, uint16_t height, uint8_t bpp);
int vga_set_mode(uint16_t width, uint16_t height, uint8_t bpp);

void vga_write_registers(const uint8_t vga_regs[61]);

// WARNING! PUTPIXEL IS NOT CHECKING FOR VIDEO BUFFER AND COORDINATES
void vga_putpixel(uint16_t x, uint16_t y, uint8_t col);

void vga_getmode(uint16_t *w, uint16_t *h, uint8_t *bpp);

void vga_fillrect(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint8_t col);
void vga_fillcircle(uint16_t x, uint16_t y, uint16_t d, uint8_t col);

void vga_draw_bitmap(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint8_t *bmap);

uint8_t *vga_get_fbuff(void);

#endif // VGA_H
