#ifndef FSUSTAR_H
#define FSUSTAR_H

#include "ata.h"

#define USTAR_MAX_PATH_LEN (155)
#define USTAR_MAX_FPATH_LEN (USTAR_MAX_PATH_LEN+100)

enum USTAR_TFLAGS
{
    USTAR_FILE=0,
    USTAR_HLINK=1,
    USTAR_SLINK=2,
    USTAR_CHDEV=3,
    USTAR_BLKDEV=4,
    USTAR_DIR=5,
    USTAR_PIPE=6,
    USTAR_EMPTY=0xA, // Empty space
};

enum SIZE_COEFFS
{
    SIZE_B=0,
    SIZE_KB=1,
    SIZE_MB=2,
    SIZE_GB=3,
    SIZE_TB=4,
};

// File entry structure (512 bytes)
struct ustar_fentry_s
{
    char fname[100];
    char fmode[8];   // r?w?x
    char ownerid[8];
    char groupid[8];
    char fsize[12];
    char lastmodif[12];
    char checksum[8];
    char tflag; // type flag (file/hardlink/symlink/etc.)
    char flinkedname[100];
    char ustari[6]; // constant
    char ustarv[2]; // constant?
    char ownername[32];
    char groupname[32];
    uint64_t drvmajver;
    uint64_t drvminver;
    char fprefix[155]; // aka filepath
    bool is_last;

    char _[11];
} __attribute__((packed));
typedef struct ustar_fentry_s ustar_fentry;

//

int ustar_newfile(const cstring fname, const cstring fpath, const cstring data, int len);
void ustar_listdir(const cstring dir, cstring out, int *count);
int ustar_fetchfile(const cstring fname, const cstring fpath, uint32_t *size, uint32_t *block);
int ustar_fetchdir(const cstring dir, const cstring path);
void ustar_getfile(uint32_t block, uint16_t sectors, cstring buffer);

int ustar_rmfile(const cstring fname, const cstring fpath);

void ustar_format(void);

uint32_t ustar_checksum(ustar_fentry *fentry);
uint16_t ustar_sectorcount(uint32_t size);

uint8_t size_coeff(uint32_t size, uint8_t adds);

#endif // FSUSTAR_H
