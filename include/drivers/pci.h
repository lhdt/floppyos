#ifndef PCI_H
#define PCI_H

#include "hw.h"

#define PCI_MAX_DEVICES (256)

struct pci_device_s
{
    uint8_t exists; // if 0xDE -> device exists

    uint8_t bus_id;
    uint8_t slot_id;
    uint8_t func_id;
    uint16_t dev_id;
    uint16_t vendor_id;
    char _[10];
    char dev_alias[16]; // at most 15 symbols for name
};
typedef struct pci_device_s pci_device;

void pci_sync(void);
uint16_t pci_read(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset);

uint8_t pci_devcount(void);
const pci_device *pci_devlistptr(void);

// 0 - got all data, 1 - missed device string, 3 - no data available
int pci_devstr(uint16_t v_id, uint16_t d_id, cstring vout, cstring dout);

int pci_init(void);

#endif // PCI_H
