#ifndef KEYBD_H
#define KEYBD_H

#include "keycodes.h"
#include "stdint.h"
#include "drivers/irq.h"

#define KB_DATA (0x60)
#define KB_STATUS (0x64)

inline char get_shifted(char c)
{
    switch(c)
    {
    case '0':
        return ')';
    case '1':
        return '!';
    case '2':
        return '@';
    case '3':
        return '#';
    case '4':
        return '$';
    case '5':
        return '%';
    case '6':
        return '^';
    case '7':
        return '&';
    case '8':
        return '*';
    case '9':
        return '(';
    case '[':
        return '{';
    case ']':
        return '}';
    case ';':
        return ':';
    case '\'':
        return '"';
    case '/':
        return '?';
    case ',':
        return '<';
    case '.':
        return '>';
    }
    if( (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
        return c^0x20;
    return 0;
}

uint8_t keybd_get_lastkey(void);
char keybd_get_lastchar(void);
void keybd_clear_lastkey(void);

void keybd_init(void);
void keybd_handle(struct regs_s *r);
void keybd_wait(void);

#endif
