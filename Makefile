TARGET=kernel.bin

CC=i686-pc-linux-gnu-gcc
LD=i686-pc-linux-gnu-ld
AS=yasm

DRIVERS=$(shell cat driver.lst)

CFLAGS=-ffreestanding -fno-stack-protector \
-I./include/ -I. \
-Os -s \
-nostdlib

OBJFILES=kernel.o elf.o lla.o syscall.o

STDLIB=libstd-i386-fos.a

%.o: src/%.c
	@$(CC) -c $< -o $@ $(CFLAGS)

all: $(OBJFILES) $(DRIVERS)
	@$(AS) -felf32 kernel.asm -o kasm.o
	@$(LD) -T link.ld -o $(TARGET) kasm.o $(DRIVERS) $(OBJFILES) $(STDLIB)

$(TARGET): all

clean:
	@rm -f $(OBJFILES) kasm.o $(DRIVERS)

distclean: clean
	@rm -f $(TARGET)

rebuild: distclean all

libs:
	@$(CC) -c src/stdlib.c -o stdlib.o $(CFLAGS)
	@$(CC) -c src/stdio.c -o stdio.o $(CFLAGS)
	@$(CC) -c src/memory.c -o memory.o $(CFLAGS)
	@$(CC) -c src/time.c -o time.o $(CFLAGS)
	@$(CC) -c src/math.c -o math.o $(CFLAGS)
	@ar cr $(STDLIB) stdlib.o stdio.o memory.o time.o math.o
	@rm -f stdlib.o stdio.o memory.o time.o math.o

drivers/%.ko: src/drivers/%.c
	@mkdir -p ./drivers/
	@$(CC) -c $< -o $@ $(CFLAGS)

drivers/net/%.ko: src/gui/%.c
	@mkdir -p ./drivers/net/
	@$(CC) -c $< -o $@ $(CFLAGS)
	
drivers/gui/%.ko: src/gui/%.c
	@mkdir -p ./drivers/gui/
	@$(CC) -c $< -o $@ $(CFLAGS)

fdd: $(TARGET)
	@mkdir -p boot
	@sudo mount ./bootgrub ./boot
	@sudo cp -f $(TARGET) ./boot/boot/kernel.bin
	@sudo umount ./boot
	@cp -f bootgrub rom.img

fuse-driver:
	@gcc -D_FILE_OFFSET_BITS=64 -I/usr/include/fuse -lfuse fs-ustar-fuse.c -o ustarfs

prim.hdd:
	@qemu-img create -fraw $@ 128M

test: all prim.hdd
	@qemu-system-i386 -hda prim.hdd -kernel $(TARGET)
