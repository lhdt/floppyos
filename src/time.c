#include "include/time.h"

extern void write_port(uint16_t port, uint8_t data);
extern uint8_t read_port(uint16_t port);
extern bool cmos_busy(void);

uint8_t get_secs()
{
    write_port(0x70, 0x00);
    return (read_port(0x71)/16) * 10;
}

uint8_t get_mins()
{
    write_port(0x70, 0x02);
    return (read_port(0x71)/16) * 10;
}

uint8_t get_hours()
{
    write_port(0x70, 0x04);
    return (read_port(0x71)/16) * 10;
}

cstring get_time_cstr()
{
    while(cmos_busy());
    uint8_t secs, mins, hours;
    secs = get_secs();
    mins = get_mins();
    hours = get_hours();

    cstring s = new_cstr(9);

    s[0] = hours/10 + '0';
    s[1] = hours%10 + '0';

    s[2] = ':';

    s[3] = mins/10 + '0';
    s[4] = mins%10 + '0';

    s[5] = ':';

    s[6] = secs/10 + '0';
    s[7] = secs%10 + '0';

    s[8] = '\0';

    return s;
}

uint32_t get_timesum()
{
    return -((uint32_t)get_hours() * 60 * 60 + (uint32_t)get_mins() * 60 + (uint32_t)get_secs());
}
