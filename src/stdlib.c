#include "include/stdlib.h"

static uint32_t rand_next = 0;

void srand(uint32_t next)
{
    rand_next = next;
}

uint32_t rand()
{
    rand_next = rand_next * 1103515245 + 12345;
    return (rand_next/65536);
}

void substr(const cstring src, int start, int len, cstring out)
{
    if(strlen(src) < (start+len))
        return;
    int it=0;

    for(int i=0; i < len; i++)
        out[it++] = src[i+start];
    out[len] = '\0';
}

int strlen(const cstring s)
{
    int len=0;
    while(s[len++])
    {
        if(len >= MAX_STRLEN)
            return len;
    }
    return (--len);
}

cstring itoa(uint32_t a, int len, int base, char fill)
{
    cstring s = new_cstr(len+1);
    if(base == 16)
    {
        static const cstring hexseq = "0123456789abcdef";
        for(int i=0, j=(len-1)*4; i < len; ++i, j-=4)
            s[i] = hexseq[(a>>j) & 0x0f];
    }
    else if(base == 10 || base == 8)
    {
        memset(s, fill, len);
        int i=0;
        do
        {
            s[i++] = (a%base) + '0';
        } while(a/=base);
        strrev(s, len);
    }
    s[len+1] = '\0';
    return s;
}

int atoi(const cstring s, int l, int base)
{
    int r=0;

    if(base == 16)
    {
        int offset=0;
        if(s[0] == '0' && (s[1]|0x20) == 'x')
            offset = 2;

        for(int i=0; i < l; i++)
        {
            char sv = s[i+offset]|0x20; // lowered value
            int addv = (sv<='9' ? (sv-'0') : (10+sv-'a'));
            r *= base;
            r += addv;
        }
    }
    else if(base == 10 || base == 8)
    {
        for(int i=0; i < l; i++)
        {
            r *= base;
            r += s[i] - '0';
        }
    }
    return r;
}

void strrev(cstring s, int len)
{
    for(int i=0; i < len/2; i++)
    {
        char a = s[i];
        s[i] = s[len-i-1];
        s[len-i-1] = a;
    }
}

int _strcmp(const cstring s1, const cstring s2, bool cis, int len)
{
    char coeff = 0;
    if(cis) // Compare case-insensitive?
        coeff = 0x20;

    if((s1[0]|coeff) != (s2[0]|coeff))
        return 1;

    if(len <= 0) // without len
    {
        int l1 = strlen(s1), l2 = strlen(s2);
        if(l1 != l2)
            return 1;
        len = l1;
    }
    for(int i=0; i < len; i++)
        if((s1[i]|coeff) != (s2[i]|coeff))
            return 1;
    return 0;
}

int strcmpi(const cstring s1, const cstring s2)
{
    return _strcmp(s1, s2, true, 0);
}

int strncmpi(const cstring s1, const cstring s2, int len)
{
    return _strcmp(s1, s2, true, len);
}

void strcpy(const cstring src, cstring dst)
{
    strncpy(src, dst, strlen(src));
}

int strcmp(const cstring s1, const cstring s2)
{
    return _strcmp(s1, s2, false, 0);
}

int strcmpp(const cstring s1, const cstring s2, int len)
{
    return _strcmp(s1, s2, false, len);
}

void strncpy(const cstring src, cstring dst, int len)
{
    for(int i=0; i < len; i++)
        dst[i] = src[i];
}

bool streqi(const string *s1, const cstring s2)
{
    return (strcmpi(s1->data, s2)==0);
}

bool streq(const string *s1, const cstring s2)
{
    return (strcmp(s1->data, s2)==0);
}

bool strneqi(const string *s1, const cstring s2, int len)
{
    return (strncmpi(s1->data, s2, len)==0);
}
