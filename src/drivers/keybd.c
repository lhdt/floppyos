#include "drivers/keybd.h"

extern void write_port(uint16_t, uint8_t);
extern uint8_t read_port(uint16_t);

static uint8_t kb_lastkey = 0;

uint8_t keybd_get_lastkey()
{
    return kb_lastkey;
}

char keybd_get_lastchar()
{
    return key_codes[kb_lastkey];
}

void keybd_init()
{
    irq_install_handler(KEYBD_IRQ, keybd_handle);
}

void keybd_handle(struct regs_s *r)
{
    (void)r;
    keybd_wait();
    kb_lastkey = read_port(KB_DATA);
    irq_ack(KEYBD_IRQ);
}

void keybd_clear_lastkey()
{
    kb_lastkey = 0;
}

void keybd_wait()
{
    while(read_port(KB_STATUS) & 0x2);
}
