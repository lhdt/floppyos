#include "drivers/psm.h"

extern void write_port(uint16_t, uint8_t);
extern uint8_t read_port(uint16_t);

static uint8_t psm_offset=0;
static int8_t psm_bytes[3];
static uint16_t psm_px=0, psm_py=0;

void psm_init()
{
    __asm__("cli");
    uint8_t status = 0;

    psm_wait(1);
    write_port(PSM_STATUS, 0xA8);
    psm_wait(1);
    write_port(PSM_STATUS, 0x20);
    psm_wait(0);
    status = read_port(PSM_DATA) | 2;
    psm_wait(1);
    write_port(PSM_STATUS, 0x60);
    psm_wait(1);
    write_port(PSM_DATA, status);

    psm_write(0xF6);
    psm_read();
    psm_write(0xF4);
    psm_read();
    __asm__("sti");

    irq_install_handler(0xC, psm_handler);
}

void psm_handler(struct regs_s *r)
{
    (void)r;

    uint8_t status = read_port(PSM_STATUS);
    while(status & 0x1)
    {
        int8_t m_data = read_port(PSM_DATA);

        if(status & 0x20)
        {
            if(psm_offset == 0)
            {
                psm_bytes[0] = m_data;
                ++psm_offset;
            }
            else if(psm_offset == 1)
            {
                psm_bytes[1] = m_data;
                ++psm_offset;
            }
            else if(psm_offset == 2)
            {
                psm_bytes[2] = m_data;
                if( (psm_bytes[0] & 0x80) || (psm_bytes[0] & 0x40)) // bad packet
                    break;

                if((int16_t)psm_px + psm_bytes[1] < 0)
                    psm_px = 0;
                else
                    psm_px += psm_bytes[1];

                if((int16_t)psm_py + psm_bytes[2] < 0)
                    psm_py = 0;
                else
                    psm_py += psm_bytes[2];
                psm_offset = 0;
                break;
            }
        }
        status = read_port(PSM_STATUS);
    }
    irq_ack(PSM_IRQ);
}

void psm_wait(uint8_t a_t)
{
    uint32_t timeout = 0x100000;
    if(a_t)
    {
        while(--timeout)
            if(!(read_port(PSM_STATUS) & 0x2))
                return;
    }
    while(--timeout)
        if((read_port(PSM_STATUS) & 0x1) == 1)
            return;
}

void psm_write(uint8_t v)
{
    psm_wait(1);
    write_port(PSM_STATUS, 0xD4);
    psm_wait(1);
    write_port(PSM_DATA, v);
}

void psm_get_pos(uint16_t *outx, uint16_t *outy)
{
    *outx = psm_px;
    *outy = psm_py;
}

uint8_t psm_read()
{
    psm_wait(0);
    // Don't do
    // return read_port(something);
    // ^^^ This doesn't work ^^^
    uint8_t d = read_port(PSM_DATA);
    return d;
}
