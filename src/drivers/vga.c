#include "drivers/vga.h"

extern void write_port(uint16_t, uint8_t);
extern uint8_t read_port(uint16_t);

static uint8_t *vga_buffptr = NULL;
static uint16_t vga_width=0, vga_height=0;
static uint8_t vga_bpp=0;

static const uint8_t vga_80x25_text_mode[61] =
{
/* MISC */
    0x67,
/* SEQ */
    0x03, 0x00, 0x03, 0x00, 0x02,
/* CRTC */
    0x5F, 0x4F, 0x50, 0x82, 0x55, 0x81, 0xBF, 0x1F,
    0x00, 0x4F, 0x0D, 0x0E, 0x00, 0x00, 0x00, 0x50,
    0x9C, 0x0E, 0x8F, 0x28, 0x1F, 0x96, 0xB9, 0xA3,
    0xFF,
/* GC */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x0E, 0x00,
    0xFF,
/* AC */
    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x14, 0x07,
    0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3F,
    0x0C, 0x00, 0x0F, 0x08, 0x00
};

static const uint8_t vga_320x200x256_mode[61] =
{
/* MISC */
    0x63,
/* SEQ */
    0x03, 0x01, 0x0F, 0x00, 0x0E,
/* CRTC */
    0x5F, 0x4F, 0x50, 0x82, 0x54, 0x80, 0xBF, 0x1F,
    0x00, 0x41, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x9C, 0x0E, 0x8F, 0x28,	0x40, 0x96, 0xB9, 0xA3,
    0xFF,
/* GC */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x05, 0x0F,
    0xFF,
/* AC */
    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
    0x41, 0x00, 0x0F, 0x00, 0x00
};

static uint8_t vga_640x400x256_mode[61] =
{
    /* MISC */
        0xE3,
    /* SEQ */
        0x03, 0x01, 0x0F, 0x00, 0x0E,
    /* CRTC */
        0x5F, 0x4F, 0x50, 0x82, 0x54, 0x80, 0xBF, 0x1F,
        0x00, 0x41, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x9C, 0x0E, 0x8F, 0xA0,	0x40, 0x96, 0xB9, 0xA7,
        0xFF,
    /* GC */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x05, 0x0F,
        0xFF,
    /* AC */
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
        0x41, 0x00, 0x0F, 0x00, 0x00
};


static const uint8_t *vga_modes[4] =
{
    vga_80x25_text_mode, vga_320x200x256_mode, vga_640x400x256_mode
};

//

bool vga_is_mode_supported(uint16_t width, uint16_t height, uint8_t bpp)
{
    return
            ((width == 320) && (height == 200) && (bpp == 8)) ||
            ((width == 640) && (height == 400) && (bpp == 8)) ||
            ((width == 80) && (height == 25));
}

int vga_set_mode(uint16_t width, uint16_t height, uint8_t bpp)
{
    if(!vga_is_mode_supported(width, height, bpp))
        return 1;

    if((width == 80) && (height == 25))
    {
        vga_write_registers(vga_80x25_text_mode);
        vga_buffptr = (uint8_t*)0xb8000;
        vga_width = 80;
        vga_height = 25;
        vga_bpp = 0;
    }
    else
    {
        if(width == 640)
        {
            vga_write_registers(vga_modes[2]);
        }
        else if(width == 320)
        {
            if(height == 200)
                vga_write_registers(vga_modes[1]);
        }

        vga_buffptr = vga_get_fbuff();
        vga_width = width;
        vga_height = height;
        vga_bpp = bpp;
    }

    return 0;
}

extern void vga_mode(uint16_t *ok);

void vga_write_registers(const uint8_t vga_regs[61])
{
    // misc
    write_port(VGA_MISC, vga_regs[0]);

    // Sequencer
    for(uint8_t i=0; i < 5; i++)
    {
        write_port(VGA_SEQ_IDX, i);
        write_port(VGA_SEQ_DATA, vga_regs[i+1]);
    }

    // unlock CRTC
    write_port(VGA_CRTC_IDX, 0x03);
    write_port(VGA_CRTC_DATA, read_port(VGA_CRTC_DATA) | 0x80);
    write_port(VGA_CRTC_IDX, 0x11);
    write_port(VGA_CRTC_DATA, read_port(VGA_CRTC_DATA) & (~0x80));
    //

    // CRTC
    for(uint8_t i=0; i < 25; i++)
    {
        write_port(VGA_CRTC_IDX, i);
        if(i == 0x03)
            write_port(VGA_CRTC_DATA, vga_regs[i]|0x80);
        else if(i == 0x11)
            write_port(VGA_CRTC_DATA, (vga_regs[i]&(~0x80)));
        else
            write_port(VGA_CRTC_DATA, vga_regs[i+6]);
    }

    // Graphics controller
    for(uint8_t i=0; i < 9; i++)
    {
        write_port(VGA_GCON_IDX, i);
        write_port(VGA_GCON_DATA, vga_regs[i+31]);
    }

    for(uint8_t i=0; i < 21; i++)
    {
        read_port(VGA_ATTR_RESET);
        write_port(VGA_ATTR_IDX, i);
        write_port(VGA_ATTR_WRITE, vga_regs[i+40]);
    }
    read_port(VGA_ATTR_RESET);
    write_port(VGA_ATTR_IDX, 0x20);
}

void vga_putpixel(uint16_t x, uint16_t y, uint8_t col)
{
    *(vga_buffptr + vga_width * y + x) = col;
}

uint8_t *vga_get_fbuff()
{
    write_port(VGA_GCON_IDX, 0x06);
    uint8_t seg = ((read_port(VGA_GCON_DATA) >> 2)&0x03);

    if(seg == 1)
        return (uint8_t*)0xA0000;
    if(seg == 2)
        return (uint8_t*)0xB0000;
    if(seg == 3)
        return (uint8_t*)0xB8000;
    return NULL;
}

void vga_fillrect(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint8_t col)
{
    if(vga_buffptr == NULL)
        return;

    for(int i=y; i < (y+h); i++)
    {
        for(int j=x; j < (x+w); j++)
        {
            if(j < vga_width && i < vga_height)
                vga_putpixel(j, i, col);
        }
    }
}

void vga_fillcircle(uint16_t x, uint16_t y, uint16_t d, uint8_t col)
{
    (void)x;
    (void)y;
    (void)d;
    (void)col;
}

void vga_getmode(uint16_t *w, uint16_t *h, uint8_t *bpp)
{
    if(w != NULL)
        *w = vga_width;
    if(h != NULL)
        *h = vga_height;
    if(bpp != NULL)
        *bpp = vga_bpp;
}


void vga_draw_bitmap(uint16_t x, uint16_t y, uint16_t w, uint16_t h, const uint8_t *bmap)
{
    for(int i=0; i < h; i++)
    {
        for(int j=0; j < w; j++)
        {
            uint8_t px = bmap[j + i*w];
            if(px)
                vga_putpixel(x+j, y+i, px);
        }
    }
}
