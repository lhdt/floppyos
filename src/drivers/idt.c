#include "drivers/idt.h"
#include "memory.h"

extern void idt_load(void);

idt_ptr_t _IDTptr;
static idt_entry_t IDT[256];

void idt_set_gate(uint8_t idt_id, uint32_t base, uint16_t selector, uint8_t flags)
{
    IDT[idt_id].base_low  = (base & 0xFFFF);
    IDT[idt_id].base_high = (base >> 16) & 0xFFFF;
    IDT[idt_id].selector = selector;
    IDT[idt_id].zero     = 0;
    IDT[idt_id].flags    = (flags | 0x60);
}

void idt_init()
{
    _IDTptr.limit = (sizeof(idt_entry_t) * 256) - 1;
    _IDTptr.base = (uint32_t)&IDT;
    memset(&IDT, 0, sizeof(idt_entry_t) * 256);

    idt_load();
}
