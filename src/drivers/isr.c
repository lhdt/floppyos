#include "drivers/isr.h"

extern void asm_isr0(void);
extern void asm_isr1(void);
extern void asm_isr2(void);
extern void asm_isr3(void);
extern void asm_isr4(void);
extern void asm_isr5(void);
extern void asm_isr6(void);
extern void asm_isr7(void);
extern void asm_isr8(void);
extern void asm_isr9(void);
extern void asm_isr10(void);
extern void asm_isr11(void);
extern void asm_isr12(void);
extern void asm_isr13(void);
extern void asm_isr14(void);
extern void asm_isr15(void);
extern void asm_isr16(void);
extern void asm_isr17(void);
extern void asm_isr18(void);
extern void asm_isr19(void);
extern void asm_isr20(void);
extern void asm_isr21(void);
extern void asm_isr22(void);
extern void asm_isr23(void);
extern void asm_isr24(void);
extern void asm_isr25(void);
extern void asm_isr26(void);
extern void asm_isr27(void);
extern void asm_isr28(void);
extern void asm_isr29(void);
extern void asm_isr30(void);
extern void asm_isr31(void);
extern void asm_isr127(void);

static irq_handler_t ISRhandlers[ISR_COUNT];

//
static const cstring exception_messages[ISR_COUNT] = {
    "Division by zero", // 0
    "Debug",
    "Non-maskable interrupt",
    "Breakpoint",
    "Detected overflow",
    "Out-of-bounds", // 5
    "Invalid opcode",
    "No coprocessor",
    "Double fault",
    "Coprocessor segment overrun",
    "Bad TSS", // 10
    "Segment not present",
    "Stack fault",
    "General protection fault",
    "Page fault",
    "Unknown interrupt", // 15
    "Coprocessor fault",
    "Alignment check",
    "Machine check",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved"
};
//

void isr_init()
{
    idt_set_gate(0x00, ISRNO(0), 0x08, 0x8e);
    idt_set_gate(0x01, ISRNO(1), 0x08, 0x8e);
    idt_set_gate(0x02, ISRNO(2), 0x08, 0x8e);
    idt_set_gate(0x03, ISRNO(3), 0x08, 0x8e);
    idt_set_gate(0x04, ISRNO(4), 0x08, 0x8e);
    idt_set_gate(0x05, ISRNO(5), 0x08, 0x8e);
    idt_set_gate(0x06, ISRNO(6), 0x08, 0x8e);
    idt_set_gate(0x07, ISRNO(7), 0x08, 0x8e);
    idt_set_gate(0x08, ISRNO(8), 0x08, 0x8e);
    idt_set_gate(0x09, ISRNO(9), 0x08, 0x8e);
    idt_set_gate(0x0A, ISRNO(10), 0x08, 0x8e);
    idt_set_gate(0x0B, ISRNO(11), 0x08, 0x8e);
    idt_set_gate(0x0C, ISRNO(12), 0x08, 0x8e);
    idt_set_gate(0x0D, ISRNO(13), 0x08, 0x8e);
    idt_set_gate(0x0E, ISRNO(14), 0x08, 0x8e);
    idt_set_gate(0x0F, ISRNO(15), 0x08, 0x8e);
    idt_set_gate(0x10, ISRNO(16), 0x08, 0x8e);
    idt_set_gate(0x11, ISRNO(17), 0x08, 0x8e);
    idt_set_gate(0x12, ISRNO(18), 0x08, 0x8e);
    idt_set_gate(0x13, ISRNO(19), 0x08, 0x8e);
    idt_set_gate(0x14, ISRNO(20), 0x08, 0x8e);
    idt_set_gate(0x15, ISRNO(21), 0x08, 0x8e);
    idt_set_gate(0x16, ISRNO(22), 0x08, 0x8e);
    idt_set_gate(0x17, ISRNO(23), 0x08, 0x8e);
    idt_set_gate(0x18, ISRNO(24), 0x08, 0x8e);
    idt_set_gate(0x19, ISRNO(25), 0x08, 0x8e);
    idt_set_gate(0x1A, ISRNO(26), 0x08, 0x8e);
    idt_set_gate(0x1B, ISRNO(27), 0x08, 0x8e);
    idt_set_gate(0x1C, ISRNO(28), 0x08, 0x8e);
    idt_set_gate(0x1D, ISRNO(29), 0x08, 0x8e);
    idt_set_gate(0x1E, ISRNO(30), 0x08, 0x8e);
    idt_set_gate(0x1F, ISRNO(31), 0x08, 0x8e);
    idt_set_gate(0x7F, ISRNO(127), 0x08, 0x8e); // syscall vector
}

void isr_install_handler(uint8_t isr_id, irq_handler_t handler)
{
    if(isr_id < 0x20 || isr_id == 0x7F)
        ISRhandlers[isr_id] = handler;
}

void isr_remove_handler(uint8_t isr_id)
{
    if(isr_id < 0x20 || isr_id == 0x7F)
        ISRhandlers[isr_id] = NULL;
}

void isr_handle(struct regs_s *r)
{
    if(r->int_id == 8)
        return;
    if(r->int_id >= 0x20 && r->int_id != 0x7F)
        return;

    __asm__("cli");
    irq_handler_t handler = ISRhandlers[r->int_id];
    if(handler)
        handler(r);
    __asm__("sti");
}

cstring isr_msg(uint8_t err)
{
    if(err >= 0x20)
        err = 0x1F;
    return exception_messages[err];
}
