#include "drivers/pci.h"
#include "lla.h"
#include "memory.h"
#include "stdlib.h"

extern void kputi(int);
extern void kputs(const cstring);
extern void kputc(char);

extern cstring itoa(uint32_t, int, int, char);

static pci_device pci_device_list[PCI_MAX_DEVICES];

void pci_sync()
{
    memset(pci_device_list, 0, sizeof (pci_device) * PCI_MAX_DEVICES); // reset array
    size_t dev_iter=0;
    for(uint32_t i=0; i < 256; i++) // buses
    {
        for(uint32_t j=0; j < 256; j++) // slots
        {
            uint16_t v_id = pci_read(i&0xff, j&0xff, 0, 0);
            if(v_id == 0xFFFF) // no device ?
                continue;

            pci_device_list[dev_iter].exists = 0xDE;
            pci_device_list[dev_iter].bus_id = i;
            pci_device_list[dev_iter].slot_id = j;
            pci_device_list[dev_iter].func_id = 0;
            pci_device_list[dev_iter].vendor_id = v_id; // vendor id
            pci_device_list[dev_iter].dev_id = pci_read(i&0xff, j&0xff, 0, 2); // device id
            dev_iter++;
        }
    }
}

uint16_t pci_read(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset)
{
    uint32_t address = (((uint32_t)bus<<16) |
                        ((uint32_t)slot << 11) |
                        ((uint32_t)func << 8) |
                        (offset & 0xfc) |
                        ((uint32_t)0x80000000));
    uint16_t tmp;

    outl(0xCF8, address);
    tmp = (uint16_t)((inl(0xCFC) >> ((offset & 2) << 3)) & 0xFFFF);
    return tmp;
}

int pci_init()
{
    pci_sync();
    return 0;
}

uint8_t pci_devcount()
{
    uint8_t dc=0;
    for(size_t i=0; i < PCI_MAX_DEVICES; i++)
        if(pci_device_list[i].exists == 0xDE)
            dc++;
    return dc;
}

const pci_device *pci_devlistptr()
{
    return pci_device_list;
}

int pci_devstr(uint16_t v_id, uint16_t d_id, cstring vout, cstring dout)
{
    if(v_id == 0x1039)
    {
        strcpy("Silicon Integrated Systems [SiS]", vout);
        if(d_id == 0x01)
        {
            strcpy("AGP Port (virtual PCI-to-PCI bridge)", dout);
            return 0;
        }
        else if(d_id == 0x08)
        {
            strcpy("SiS85C503/5513 (LPC Bridge)", dout);
            return 0;
        }
        else if(d_id == 0x0630)
        {
            strcpy("630 Host", dout);
            return 0;
        }
        else if(d_id == 0x6300)
        {
            strcpy("630/730 PCI/AGP VGA Display Adapter", dout);
            return 0;
        }
        return 1;
    }
    if(v_id == 0x1234)
    {
        strcpy("Technical Corporation", vout);
        if(d_id == 0x1111)
        {
            strcpy("QEMU virtual VGA", dout);
            return 0;
        }
        return 1;
    }
    else if(v_id == 0x8086)
    {
        strcpy("Intel Corporation", vout);
        if(d_id == 0x100E)
        {
            strcpy("82540EM Gigabit Ethernet Controller", dout);
            return 0;
        }
        else if(d_id == 0x1237)
        {
            strcpy("440FX - 82441FX PMC [Natoma]", dout);
            return 0;
        }
        else if(d_id == 0x7000)
        {
            strcpy("82371SB PIIX3 ISA [Natoma/Triton II]", dout);
            return 0;
        }
        return 1;
    }
    return 3;
}
