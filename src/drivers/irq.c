#include "drivers/irq.h"
#include "drivers/idt.h"
#include "memory.h"

extern void keyboard_handler(void);
extern void mouse_handler(void);

extern void write_port(uint16_t, uint8_t);
extern uint8_t read_port(uint16_t);
extern void load_idt(uint32_t*);

extern void asm_irq0(void);
extern void asm_irq1(void);
extern void asm_irq2(void);
extern void asm_irq3(void);
extern void asm_irq4(void);
extern void asm_irq5(void);
extern void asm_irq6(void);
extern void asm_irq7(void);
extern void asm_irq8(void);
extern void asm_irq9(void);
extern void asm_irq10(void);
extern void asm_irq11(void);
extern void asm_irq12(void);
extern void asm_irq13(void);
extern void asm_irq14(void);
extern void asm_irq15(void);

// Static variables

// idt
static irq_handler_t IRQhandlers[16];

// mouse
static uint16_t mouse_x=0, mouse_y=0;
static uint8_t mouse_buffer[3];
static uint8_t mouse_offset=0;

// keyboard
static char keyb_last_key=0;

//

void irq_install_handler(uint8_t irq_id, irq_handler_t handle)
{
    if(irq_id < 0x10)
        IRQhandlers[irq_id] = handle;
}

void irq_remove_handler(uint8_t irq_id)
{
    if(irq_id < 0x10)
        IRQhandlers[irq_id] = NULL;
}

void irq_map()
{
    write_port(0x20, 0x11);
    write_port(0xA0, 0x11);
    write_port(0x21, 0x20);
    write_port(0xA1, 0x28);
    write_port(0x21, 0x04);
    write_port(0xA1, 0x02);
    write_port(0x21, 0x01);
    write_port(0xA1, 0x01);
    write_port(0x21, 0x00);
    write_port(0xA1, 0x00);
}

void irq_init()
{
    irq_map();
    irq_gates();
    __asm__("sti");
}

void irq_ack(uint8_t irq_id)
{
    if(irq_id >= 0x0C)
        write_port(0xA0, 0x20);
    write_port(0x20, 0x20);
}

void irq_gates()
{
    idt_set_gate(0x20, IRQNO(0),  0x08, 0x8e); // 32
    idt_set_gate(0x21, IRQNO(1),  0x08, 0x8e);
    idt_set_gate(0x22, IRQNO(2),  0x08, 0x8e);
    idt_set_gate(0x23, IRQNO(3),  0x08, 0x8e);
    idt_set_gate(0x24, IRQNO(4),  0x08, 0x8e);
    idt_set_gate(0x25, IRQNO(5),  0x08, 0x8e);
    idt_set_gate(0x26, IRQNO(6),  0x08, 0x8e);
    idt_set_gate(0x27, IRQNO(7),  0x08, 0x8e);
    idt_set_gate(0x28, IRQNO(8),  0x08, 0x8e);
    idt_set_gate(0x29, IRQNO(9),  0x08, 0x8e);
    idt_set_gate(0x2A, IRQNO(10), 0x08, 0x8e);
    idt_set_gate(0x2B, IRQNO(11), 0x08, 0x8e);
    idt_set_gate(0x2C, IRQNO(12), 0x08, 0x8e);
    idt_set_gate(0x2D, IRQNO(13), 0x08, 0x8e);
    idt_set_gate(0x2E, IRQNO(14), 0x08, 0x8e);
    idt_set_gate(0x2F, IRQNO(15), 0x08, 0x8e); // 47
}

void irq_handle(struct regs_s *r)
{
    __asm__("cli");
    if(r->int_id > 47 || r->int_id < 32)
        return;

    irq_handler_t handler = IRQhandlers[r->int_id - 0x20];
    if(handler)
        handler(r);
    else
        irq_ack(r->int_id - 0x20);
    __asm__("sti");
}
