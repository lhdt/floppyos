#include "drivers/ata.h"

extern void write_port(uint16_t port, uint8_t data);
extern void write_port_word(uint16_t port, uint16_t data);
extern uint8_t read_port(uint16_t port);
extern uint16_t read_port_word(uint16_t port);

extern void kputc(char);
extern void kputs(const cstring);
extern int term_curline(void);
extern void term_clrline(int);
extern void kputi(int);

//
static ATA_info hdd_infotable[4];
//

int hdd_readblk(bool primary, uint8_t drive, uint32_t baddr, cstring data)
{
    if(!hdd_infotable[(primary?0:2)+drive].connected)
        return 3;
    uint16_t port = (primary?0x1f0:0x170);

    write_port(port+6, (uint8_t)(0xe0 | (drive<<4) | ((baddr >> 24)&0x0f))); // @baddr (bits 24-27)
    write_port(port+1, 0x00);
    write_port(port+2, 1); // 1 sector (512 bytes)
    write_port(port+3, (uint8_t)(baddr)); // @baddr (bits 0-7)
    write_port(port+4, (uint8_t)(baddr>>8)); // @baddr (bits 8-15)
    write_port(port+5, (uint8_t)(baddr>>16)); // @baddr (bits 16-23)
    write_port(port+7, LBA28_READR);

    //
    uint8_t lst=0;
    uint8_t stat;
    while(!(stat=hdd_ready(primary)))
    {
        if(stat != lst)
        {
            lst = stat;
            kputi(stat);
            kputc('\n');
        }
    }
    //

    if(hdd_status(primary)&0x01)
        return 2; // disk error

    for(int i=0; i < 256; i++)
    {
        uint16_t val;
        val = read_port_word(port);
        data[2*i] = val&0xff;
        data[(2*i)+1] = (val>>8)&0xff;
    }
    return 0;
}

int hdd_writeblk(bool primary, uint8_t drive, uint32_t baddr, const cstring data)
{
    if(!hdd_infotable[(primary?0:2)+drive].connected)
        return 3;
    uint16_t port = (primary?0x1f0:0x170);

    write_port(port+6, (uint8_t)(0xe0 | (drive<<4) | ((baddr >> 24)&0x0f))); // @baddr (bits 24-27)
    write_port(port+1, 0x00); // NUL
    write_port(port+2, 1); // 1 sector (512 bytes)
    write_port(port+3, (uint8_t)(baddr)); // @baddr (bits 0-7)
    write_port(port+4, (uint8_t)(baddr>>8)); // @baddr (bits 8-15)
    write_port(port+5, (uint8_t)(baddr>>16)); // @baddr (bits 16-23)
    write_port(port+7, LBA28_WRITER);

    //
    uint8_t lst=0;
    uint8_t stat;
    while(!(stat=hdd_ready(primary)))
    {
        if(stat != lst)
        {
            lst = stat;
            kputi(stat);
            kputc('\n');
        }
    }
    //

    if(hdd_status(primary)&0x01)
        return 2; // disk error

    for(int i=0; i < 256; i++)
    {
        uint16_t tmp = (uint16_t)data[2*i] | (((uint16_t)data[(2*i)+1])<<8);
        write_port_word(port, tmp);
    }

    return 0;
}

int hdd_format(bool primary, uint8_t drive)
{
    // ToDo with DMA
    (void)primary;
    (void)drive;
    return 1;
}


bool hdd_ready(bool primary)
{
    uint8_t drq = hdd_status(primary)&0x08;
    return drq;
}

uint8_t hdd_status(bool primary)
{
    return (read_port((primary?ATA_PRIMARY_BASE_PORT:ATA_SECONDARY_BASE_PORT)+7));
}

bool hdd_exists(bool primary, uint8_t driveid)
{
    uint16_t port;
    if(primary)
        port = 0x1f3;
    else
        port = 0x173;

    // Check controller
    write_port(port, 0x88);
    if(read_port(port) != 0x88)
        return false;
    //

    // Check drive
    write_port(port+3, (driveid<<4));
    for(uint32_t i=0; i < 0x1000; i++) // wait some time
        read_port(0x80);

    if(!(read_port(port+4)&0x40))
        return false;
    //
    return true;
}

int hdd_info(bool primary, uint8_t drive, uint32_t *blockcount)
{
    if(!hdd_exists(primary, drive))
    {
        *blockcount = 0; // error
        return 1;
    }

    uint16_t port = (primary?0x1f0:0x170);

    write_port(port+6, (drive<<4));
    write_port(port+1, 0);
    write_port(port+2, 0);
    write_port(port+3, 0);
    write_port(port+4, 0);
    write_port(port+5, 0);
    write_port(port+7, LBA28_IDENTIFY);

    // delay
    int cnt=0;
    while(read_port(0x80))
    {
        if(cnt++ >= 0x20000)
            break;
    }
    //
    if(!(hdd_status(primary)&0x40)) // Check RDY bit
        return 3;

    *blockcount = 0;
    for(int i=0; i < 256; i++)
    {
        if(i == 60)
        {
            *blockcount |= read_port_word(port);
        }
        else if(i == 61)
        {
            *blockcount |= (((uint32_t)read_port_word(port)) << 16);
        }
        else
            read_port_word(port);
    }
    *blockcount &= 0x0FFFFFFF; // limit
    return 0;
}

int hdd_sync()
{
    uint32_t blkcount;

    int r=0, did=0; // diskid
    for(int i=0; i < 2; i++)
    {
        for(int j=0; j < 2; j++)
        {
            r = hdd_info((i==0?true:false), j, &blkcount);

            uint8_t adds=0;
            if(blkcount > 0)
            {
                if(blkcount >= 1024)
                {
                    blkcount >>= 10;
                    adds++;
                    if(blkcount >= 1024)
                    {
                        blkcount >>= 10;
                        adds++;
                        if(blkcount >= 1024)
                        {
                            blkcount >>= 10;
                            adds++;
                        }
                    }
                }
                // Write to infotable
                memcpy((r==0?" HDD":"None"), hdd_infotable[did].type, 4);
                hdd_infotable[did].type[4] = '\0';
                hdd_infotable[did].blocks = blkcount;
                hdd_infotable[did].cgrade = adds;
                hdd_infotable[did].connected = (r==0?true:false);
                did++;
                //
            }
        }
    }
    return 0;
}

ATA_info *hdd_infostructptr()
{
    return hdd_infotable;
}
