#include "drivers/fs-ustar.h"

extern void kputs(const cstring);
extern void kputsn(const cstring, int len);
extern void kputc(char c);
extern void kputi(int a);

int ustar_newfile(const cstring fname, const cstring fpath, const cstring data, int len)
{
    hdd_sync();

    ATA_info *hddinfoptr = hdd_infostructptr();

    if(!hddinfoptr[0].connected)
        return 3;

    cstring eptr = new_cstr(512);
    ustar_fentry *entry = (ustar_fentry*)eptr;

    int flen = strlen(fname);
    strncpy(fname, entry->fname, flen);

    memcpy("000666", entry->fmode, 6); // everyone can read/write
    entry->fmode[6] = ' ';
    entry->fmode[7] = 0;

    memcpy("001750", entry->ownerid, 6); // user
    entry->ownerid[6] = ' ';
    entry->ownerid[7] = 0;

    memcpy("001750", entry->groupid, 6); // user
    entry->groupid[6] = ' ';
    entry->groupid[7] = 0;

    cstring fsize = itoa(len, 11, 8, '0');
    memcpy(fsize, entry->fsize, 12);
    del_cstr(fsize, 12);

    memset(entry->lastmodif, '0', 11);
    entry->lastmodif[11] = 0;

    entry->tflag = USTAR_FILE+'0';

    memset(entry->flinkedname, 0, 100);

    // ustar sign and version
    memcpy("ustar", entry->ustari, 6);
    memset(entry->ustarv, ' ', 2);
    //

    memset(entry->ownername, 0, 32);
    memset(entry->groupname, 0, 32);

    entry->drvmajver = 0;
    entry->drvminver = 0;

    int plen = strlen(fpath);
    if(plen > 1)
    {
        if(fpath[plen-1] == '/')
            fpath[plen-1] = '\0'; // remove last slash
    }
    strcpy(fpath, entry->fprefix);

    uint32_t hddcap = (hddinfoptr[0].blocks << (10*hddinfoptr[0].cgrade));

    ustar_fentry fentry;
    cstring feptr = (cstring)&fentry;
    uint32_t blk=0;
    do
    {
        hdd_readblk(true, 0, blk, feptr);
        if(fentry.fname[0] == '\0')
            break;
        uint32_t size = atoi(fentry.fsize, 11, 8);
        blk += ustar_sectorcount(size) + 1;
    }while(blk < hddcap);

    if(blk >= hddcap)
        return 1;

    uint32_t chsum = ustar_checksum(entry);
    cstring chsumstr = itoa(chsum, 6, 8, '0');

    memcpy(chsumstr, entry->checksum, 6);
    entry->checksum[6] = 0;
    entry->checksum[7] = ' ';

    int r=0;

    r = hdd_writeblk(true, 0, blk, eptr);
    del_cstr(eptr, 512);
    if(r == 3)
        return 1; // just error

    uint32_t scount = ustar_sectorcount(len);

    cstring databuff = new_cstr(ATA_BLOCKSIZE * scount);
    memcpy(data, databuff, len);
    uint32_t i;
    for(i=0; i < scount; i++)
    {
        r = hdd_writeblk(true, 0, (blk+1+i), (databuff+ATA_BLOCKSIZE*i));
        if(r)
            return 1;
    }
    del_cstr(databuff, ATA_BLOCKSIZE * scount);

    cstring zdata = new_cstr(ATA_BLOCKSIZE);
    ustar_fentry *zerobuff = (ustar_fentry*)zdata;

    i++; // go to next block
    cstring fsizestr = itoa((hddcap-blk-i)<<9, 11, 8, ' '); // write size in bytes
    strcpy(fsizestr, zerobuff->fsize);

    memcpy("ustar", zerobuff->ustari, 5);
    memset(zerobuff->ustarv, ' ', 2);

    hdd_writeblk(true, 0, (blk+i), zdata);
    del_cstr(zdata, ATA_BLOCKSIZE);
    return 0;
}

void ustar_listdir(const cstring dir, cstring out, int *count)
{
    if(strlen(dir) == 0) // empty dirname
        return;

    ATA_info *hddinfoptr = hdd_infostructptr();
    if(!hddinfoptr[0].connected)
        return;

    int posptr=0;
    if(strcmp(dir, "/") == 0) // root directory
    {
        out[0] = '.';
        out[1] = '\0';
        *count = 1; // only '.', not '..'

        posptr+=2;
    }
    else
    {
        // curdir
        out[0] = '.';
        out[1] = '\0';

        // upperdir
        out[2] = '.';
        out[3] = '.';
        out[4] = '\0';
        *count = 2; // only '.', not '..'

        posptr += 5;
    }

    // limit to 16 blocks (at most 8 folders)
    ustar_fentry fentry;
    cstring eptr = (cstring)&fentry;

    static cstring bstr = "Bytes";

    uint32_t hddcap = (hddinfoptr[0].blocks << (10*hddinfoptr[0].cgrade));

    for(uint32_t i=0; i < hddcap; i++)
    {
        hdd_readblk(true, 0, i, eptr);

        if(strcmpi(fentry.ustari, "ustar") != 0)
        {
            kputs("Broken filesystem at block: ");
            kputi(i);
            kputs("\nPlease format drive\n");
            break;
        }

        uint32_t fsize = atoi(fentry.fsize, 11, 8);

        if(fsize == 0) // no data
        {
            break; // for now
        }

        if(fentry.tflag == USTAR_EMPTY)
        {
            i += fsize;
            continue;
        }

        i += ustar_sectorcount(fsize);

        if(strcmpi(dir, fentry.fprefix) != 0)
            continue;

        int fnlen = strlen(fentry.fname);
        strcpy(fentry.fname, (out+posptr));

        cstring fsizestr = itoa(fsize, 4, 10, ' ');
        out[posptr+fnlen] = ' ';
        out[posptr+fnlen+5] = ' ';
        if(fsize > 1024)
        {
            fsize /= 1024;
            out[posptr+fnlen+6] = 'K';
        }
        else
            out[posptr+fnlen+6] = ' ';
        strncpy(fsizestr, (out+posptr+fnlen+1), 4);
        del_cstr(fsizestr, 5);
        strcpy(bstr, (out+posptr+fnlen+7));

        posptr += fnlen+12+1; // "name size xBytes\0"
        (*count)++;
    }
}

uint32_t ustar_checksum(ustar_fentry *fentry)
{
    memset(fentry->checksum, ' ', 8);
    uint32_t sum = 0;

    uint8_t *rawptr = (uint8_t*)fentry;

    for(int i=0; i < 512; i++)
        sum += rawptr[i];
    return sum;
}

uint16_t ustar_sectorcount(uint32_t size)
{
    if(size % 512 == 0) // 512, 1024, etc.
        return (size/512); // skip current inode + data sectors
    else
        return (size/512)+1; // current inode + add. data sectors
}

int ustar_fetchfile(const cstring fname, const cstring fpath, uint32_t *size, uint32_t *block)
{
    uint32_t hddcap = (hdd_infostructptr()[0].blocks << (10*hdd_infostructptr()[0].cgrade));
    cstring fdata = new_cstr(ATA_BLOCKSIZE);
    ustar_fentry *fentry = (ustar_fentry*)fdata;
    uint32_t i;
    *size=0; // reset size
    for(i=0; i < hddcap; i++) // at most 256 blocks (for now)
    {
        hdd_readblk(true, 0, i, (cstring)fentry);
        if(strcmpi(fentry->ustari, "ustar") != 0)
        {
            *size=0; // failed to fetch file
            del_cstr(fdata, ATA_BLOCKSIZE);
            return 2; // broken FS
        }
        uint32_t cursize = atoi(fentry->fsize, 11, 8);

        if(fentry->tflag == USTAR_EMPTY || fentry->tflag == (USTAR_EMPTY+'0'))
        {
            i += cursize;
            continue;
        }

        if((strcmpi(fpath, fentry->fprefix) == 0) && (strcmpi(fname, fentry->fname) == 0))
        {
            *size = cursize;
            *block = i;
            break;
        }
        else
            i += ustar_sectorcount(cursize);
    }
    del_cstr(fdata, ATA_BLOCKSIZE);
    if(i == hddcap) // file not found
        return 3;
    return 0;
}

int ustar_fetchdir(const cstring dir, const cstring path)
{
    if(strcmpi(dir, ".") == 0)
        return 1; // don't change folder, don't issue error
    if(strcmpi(dir, "..") == 0) // Not implemented yet
        return 1;
    //
    uint32_t hddcap = (hdd_infostructptr()[0].blocks << (10*hdd_infostructptr()[0].cgrade));
    ustar_fentry dentry;
    cstring eptr = (cstring)&dentry;
    uint32_t i=0;
    for(i=0; i < hddcap; i++)
    {
        if(hdd_readblk(true, 0, i, eptr))
            return 1;
        if(strcmpi(dentry.ustari, "ustar") != 0)
            return 2;

        uint32_t dsize = atoi(dentry.fsize, 11, 8);

        if(dentry.tflag == USTAR_EMPTY || dentry.tflag == (USTAR_EMPTY+'0'))
        {
            i += dsize; // dsize for this type is number of blocks
            continue;
        }

        if(dentry.tflag != USTAR_DIR && dentry.tflag != (USTAR_DIR+'0'))
        {
            i += ustar_sectorcount(dsize);
            continue;
        }
        if((strcmpi(path, dentry.fprefix)==0) && (strcmpi(dir, dentry.fname)==0))
             break;
    }
    if(i == hddcap)
        return 3;
    return 0;
}

// block is inode block, so data block goes next
void ustar_getfile(uint32_t block, uint16_t sectors, cstring buffer)
{
    for(int i=0; i < sectors; i++)
        hdd_readblk(true, 0, (block+1+i), (buffer + (ATA_BLOCKSIZE*i)));
}

uint8_t size_coeff(uint32_t size, uint8_t adds)
{
    uint8_t sz=0;
    if(size >= 1024)
    {
        size >>= 10;
        if(size >= 1024)
        {
            size >>= 10;
            if(size >= 1024)
                sz=SIZE_GB;
            else sz=SIZE_MB;
        }
        else sz=SIZE_KB;
    } else sz=SIZE_B;
    return (sz+adds);
}


int ustar_rmfile(const cstring fname, const cstring fpath)
{
    uint32_t fsize, fblk;
    int r;

    r = ustar_fetchfile(fname, fpath, &fsize, &fblk);
    if(r)
        return r;

    int scount = ustar_sectorcount(fsize);

    cstring zdata = new_cstr(ATA_BLOCKSIZE);
    ustar_fentry *zentry = (ustar_fentry*)zdata;

    cstring fsizes = itoa(scount, 11, 8, '0');
    strcpy(fsizes, zentry->fsize);
    del_cstr(fsizes, 12);

    memcpy("ustar", zentry->ustari, 5);
    memset(zentry->ustarv, ' ', 2);

    hdd_writeblk(true, 0, fblk, zdata);

//    for(int i=0; i < (scount+1); i++) // erase inode + file content
//        hdd_writeblk(true, 0, fblk+i, zdata+ATA_BLOCKSIZE*i);

    del_cstr(zdata, ATA_BLOCKSIZE);

    return 0;
}

void ustar_format()
{
    hdd_sync();

    uint32_t hddcap = (hdd_infostructptr()[0].blocks << (10 * hdd_infostructptr()[0].cgrade));

    cstring blkdata = new_cstr(ATA_BLOCKSIZE);
    ustar_fentry *entry = (ustar_fentry*)blkdata;
    entry->tflag = USTAR_EMPTY;
    memcpy("ustar", entry->ustari, 5);
    memset(entry->ustarv, ' ', 2);

    cstring fsizes = itoa((hddcap-1), 11, 8, '0');
    strcpy(fsizes, entry->fsize);
    del_cstr(fsizes, 12);

    hdd_writeblk(true, 0, 0, blkdata);

    del_cstr(blkdata, ATA_BLOCKSIZE);
}
