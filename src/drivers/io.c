#include "drivers/io.h"
#include "drivers/keybd.h"

extern void write_port(uint16_t, uint8_t);
extern int powi(int, int);

static uint16_t *const vidmem_ptr = (uint16_t*)0xb8000;
static uint16_t cursor_x=0, cursor_y=0;
static uint16_t term_color=0x0700;
static const uint16_t scr_width = 80, scr_height = 25;

//
int hlen(uint32_t a)
{
    int len=0;
    do
    {
        len++;
    } while(a/=16);
    return len;
}

int ilen(int a)
{
    int len=0;
    do
    {
        len++;
    } while(a/=10);
    return len;
}

//

void kclear()
{
    for(uint16_t i=0; i < scr_width*scr_height; vidmem_ptr[i++]=term_color);
    cursor_x = cursor_y = 0;
}

uint16_t kgetline()
{
    return cursor_y;
}

void kclearline(uint16_t line)
{
    for(uint16_t i=0; i < scr_width; i++)
    {
        vidmem_ptr[ line * scr_width + i ] = term_color;
    }
}

void kputs(cstring s)
{
    kputsn(s, strlen(s));
}

void kputi(int a)
{
    uint8_t neg = false;
    if(a < 0)
    {
        a = -a;
        neg = true;
    }

    int len = ilen(a);

    if(neg)
        kputc('-');
    for(int i=0; i < len; i++)
    {
        kputc( '0' + (a/powi(10, len-i-1)%10) );
    }
}

void kputh(uint32_t a)
{
    uint8_t res=0;
    int len = hlen(a);

    for(int i=0; i < len; i++)
    {
        res = (a / powi(16, len-i-1))%16;

        if(res >= 10)
            kputc( 'A' + (res-10));
        else
            kputc( '0' +  res);
    }
}

void kputc(char c)
{
    if(c == '\0')
        return;

    if(c == '\n')
    {
        knewline();
        kupdcur();
        return;
    }
    else if(c == '\t')
    {
        cursor_x += 4;

        kupdcur();
        return;
    }
    else if(c == '\r')
    {
        cursor_x = 0;
        kupdcur();
        return;
    }
    else if(c == 0x08) // Backspace (reversed delete)
    {
        if(cursor_x>0)
            cursor_x--;
        vidmem_ptr[cursor_x + cursor_y*scr_width] = term_color | 0x20; // whitespace
        kupdcur();
        return;
    }
    vidmem_ptr[cursor_x + cursor_y*scr_width] = term_color | c;

    cursor_x++;
    kchkcur();
    kupdcur();
}

void kscroll()
{
    for(uint16_t i=0; i < scr_width*(scr_height-1); i++) // minus first, minus last
        vidmem_ptr[i] = vidmem_ptr[i + scr_width];

    kclearline(scr_height - 1);
    if(cursor_y > 0)
    {
        cursor_x = 0;
        cursor_y--;
    }
}

void kchkcur()
{
    if(cursor_x >= scr_width)
    {
        cursor_x = 0;
        cursor_y++;
    }
    if(cursor_y >= scr_height)
    {
        kscroll();
    }
}

void knewline()
{
    cursor_x = 0;
    cursor_y++;
    kchkcur();
}

void kupdcur()
{
    uint16_t cpos = cursor_y * scr_width + cursor_x;

    write_port(0x3d4, 14);
    write_port(0x3d5, cpos >> 8);
    write_port(0x3d4, 15);
    write_port(0x3d5, cpos);
}

void ksetcol(uint8_t col)
{
    term_color = ((uint16_t)col<<8)&0xff00;
}

void kputcc(char c, uint8_t col)
{
    uint16_t tmp = term_color;

    ksetcol(col);
    kputc(c);

    term_color = tmp;
}

void kputsn(cstring s, int len)
{
    for(int i=0; i < len; kputc(s[i++]));
}

void kputstr(string *s)
{
    kputs(s->data);
}

uint8_t kgetcol()
{
    return (term_color>>8);
}

void kgets(string *buffstr, bool echo)
{
    uint8_t i=0;
    uint8_t sread = 1, shifted = false;

    while(sread)
    {
        uint8_t k = kgetk();

        if((k & 128) == 128) // key up
        {
            if(k == 0xAA)
                shifted = false;
            continue;
        }

        char c = key_codes[k];

        if(c == 0x18)
        {
            shifted = true;
            continue;
        }

        if(c == '\n')
            sread = false;
        else if(c == '\b')
        {
            if(i >= 1)
                buffstr->data[--i] = ' ';
            else
                continue;
        }
        else
            buffstr->data[i++] = c;

        if(echo)
            kputc(c);
    }
    buffstr->data[i] = '\0'; // Zero-ending string
}

uint8_t kgetk()
{
    uint8_t oldk = keybd_get_lastkey();
    while(1)
    {
        uint8_t newk = keybd_get_lastkey();
        if(newk != oldk) // key changed (pressed?)
        {
            oldk = newk;
            keybd_clear_lastkey();
            break;
        }
    }
    return oldk;
}

char kgetc()
{
    return key_codes[kgetk()];
}
