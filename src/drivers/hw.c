#include "drivers/hw.h"
#include "memory.h"

extern void tss_flush(void);
extern void gdt_flush(void);

static gdt_entry_t GDT[6];
gdt_ptr_t _GDTptr; // NON-STATIC
static tss_entry_t tss_entry;

void gdt_init()
{
    _GDTptr.limit = (sizeof(gdt_entry_t) * 6) - 1;
    _GDTptr.base = (uint32_t)&GDT;

    // NULL
    gdt_set_gate(0, 0, 0, 0, 0);
    // Code
    gdt_set_gate(1, 0, 0xFFFFFFFF, 0x9A, 0xCF);
    // Data
    gdt_set_gate(2, 0, 0xFFFFFFFF, 0x92, 0xCF);
    // User code
    gdt_set_gate(3, 0, 0xFFFFFFFF, 0xFA, 0xCF);
    // User data
    gdt_set_gate(4, 0, 0xFFFFFFFF, 0xF2, 0xCF);

    tss_write(5, 0x10, 0x00);

    gdt_flush();
    tss_flush();
}

void gdt_set_gate(uint8_t gdt_id, uint32_t base, uint32_t limit, uint8_t access, uint8_t gran)
{
    if(gdt_id < 6)
    {
        GDT[gdt_id].base_low  = (base & 0xFFFF);
        GDT[gdt_id].base_mid  = (base >> 16) & 0xFF;
        GDT[gdt_id].base_high = (base >> 24) & 0xFF;

        GDT[gdt_id].limit_low = (limit & 0xFFFF);
        GDT[gdt_id].gran = (limit >> 16) & 0x0F;
        GDT[gdt_id].gran |= (gran & 0xF0);
        GDT[gdt_id].access = access;
    }
}

void tss_init(gdt_entry_t *g)
{

}

void set_kernel_stack(uint32_t stack)
{
    tss_entry.esp0 = stack;
}

void tss_write(uint32_t tss_id, uint16_t ss0, uint32_t esp0)
{
    uint32_t base = (uint32_t)&tss_entry;
    uint32_t limit = base + sizeof(tss_entry);

    gdt_set_gate(tss_id, base, limit, 0xE9, 0x00);

    tss_entry.ss0 = ss0;
    tss_entry.esp0 = esp0;

    tss_entry.cs = 0x00;
    tss_entry.ss =
            tss_entry.ds =
            tss_entry.es =
            tss_entry.fs =
            tss_entry.gs = 0x13;
    tss_entry.iomap_base = sizeof(tss_entry);
}
