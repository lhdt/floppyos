#include "include/math.h"


float radf(float _deg)
{
    return (_deg * RAD_IN_DEGf);
}

float degf(float _rad)
{
    return ( _rad * DEG_IN_RADf );
}

float powf(float x, int a)
{
    float r = 1;
    if(a < 0)
        return 0.f;
    while(a-- > 0)
        r *= x;
    return r;
}

int powi(int x, int a)
{
    int r = 1;
    if(a < 0)
        return 0;
    while(a-- > 0)
        r*=x;
    return r;
}

float sinf(float a)
{
    static float prec = 0.000001f;

    int gr=1;

    bool sw=false;

    float r=0.f;
    float prev=0.f;
    do
    {
        r += (sw?1.f:-1.f) * powf(a, gr) / fac(gr);
        gr += 2;
        sw = !sw;
        if(absf(prev - r) > prec)
            break;
        prev = r;
    } while(1);
    return r;
}

float absf(float a)
{
    if(a<0.f)
        return -a;
    return a;
}

int fac(int a)
{
    if(a < 0)
        return 0;
    int r = 1;
    while(a-- > 0)
        r *= a;
    return r;
}

int abs(int a)
{
    return (a<0?-a:a);
}
