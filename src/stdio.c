#include "stdio.h"
#include "stdlib.h"
#include "syscall.h"
#include "lla.h"

void putc(char c)
{
    set_eax(CALL_PUTC);
    set_ebx(c);
    SYSCALL;
}

void puts(cstring s)
{
    set_eax(CALL_PUTS);
    set_esi((uint32_t)s);
    set_edi(strlen(s));
    SYSCALL;
}

void puti(int a)
{
    set_eax(CALL_PUTI);
    set_ebx(a);
    SYSCALL;
}

void puth(uint32_t a)
{
    set_eax(CALL_PUTH);
    set_ebx(a);
    SYSCALL;
}
