#include "drivers/keybd.h"
#include "drivers/psm.h" // ps/2 mouse
#include "drivers/fs-ustar.h"
#include "include/stdlib.h"
#include "drivers/hw.h"

#include "drivers/idt.h"
#include "drivers/isr.h"
#include "drivers/irq.h"

#include "time.h"
#include "math.h"
#include "drivers/vga.h"
#include "gui/hcore.h"
#include "drivers/pci.h"
#include "drivers/io.h"
#include "elf.h"

//
#define CMD_CSTR_LEN (64)
#define PASSWD_MAX_LEN (64)

// #undef FOS_READONLY

extern void keyboard_handler(void);
extern uint8_t read_port(uint16_t);
extern void write_port(uint16_t, uint8_t);
extern void write_port_word(uint16_t, uint16_t);
extern void load_idt(uint32_t *);


//
void shutdown()
{
    write_port_word(0xB004, 0x2000); // qemu old
    write_port_word(0x0604, 0x2000); // qemu new
    write_port_word(0x4004, 0x3400); // vbox
    __asm__("hlt"); // PC
}

void reboot()
{
    uint8_t good = 0x02;
    while (good & 0x02)
        good = read_port(0x64);
    write_port(0x64, 0xFE);
}

//

int cmos_busy()
{
    write_port(0x70, 0x0A);
    return (read_port(0x71)&0x80);
}

uint16_t cmos_ram()
{
    uint16_t total;
    uint8_t lowmem, highmem;
    
    write_port(0x70, 0x30);
    lowmem = read_port(0x71);
    write_port(0x70, 0x31);
    highmem = read_port(0x71);
    
    total = (uint16_t)lowmem | ((uint16_t)highmem << 8);
    
    return total;
}

uint32_t cpu_spd()
{
//    write_port(0x43, 0x34);
//    write_port(0x40, 0);
//    write_port(0x40, 0);

//    for(int i=0x1000; i > 0; i--);

//    write_port(0x43, 0x04);
//    uint8 low  = read_port(0x40);
//    uint8 high = read_port(0x40);

//    uint32 ticks = (0x10000 - (((uint32)high<<8) + (uint32)low));

//    return 1193180 / ticks;
    return 0;
}

void dumppci()
{
    uint8_t pci_devc = pci_devcount();
    const pci_device *devptr = pci_devlistptr();

    cstring vendstrbuff = new_cstr(64);
    cstring devstrbuff = new_cstr(64);

    for(uint8_t i=0; i < pci_devc; i++)
    {
        kputh(devptr[i].bus_id);
        kputc(':');
        kputh(devptr[i].slot_id);
        kputc('.');
        kputh(devptr[i].func_id);

        kputc('\t');

        int r = pci_devstr(devptr[i].vendor_id, devptr[i].dev_id, vendstrbuff, devstrbuff);
        if(r == 0)
        {
            kputs(vendstrbuff);
            kputc('\t');
            kputs(devstrbuff);
        }
        else if(r == 1)
        {
            kputs(vendstrbuff);
            kputs(" [#");
            kputh(devptr[i].dev_id);
            kputc(']');
        }
        else
        {
            kputc('{');
            kputh(devptr[i].vendor_id);
            kputc(':');
            kputh(devptr[i].dev_id);
            kputc('}');
        }
        kputc('\n');
    }
    del_cstr(devstrbuff, 64);
    del_cstr(vendstrbuff, 64);
}

/*
 * @root - directory of user.conf (in)
 * @username - pointer to username (out)
 * @curpath - pointer to current path (out)
*/
void load_cfg(cstring root, string *username, string *curpath)
{
    uint32_t csize, cblk;
    if(ustar_fetchfile("user.conf", root, &csize, &cblk) == 0)
    {
        uint32_t scount = ustar_sectorcount(csize);
        uint32_t cbuffsize = scount * ATA_BLOCKSIZE;
        cstring cbuff = new_cstr(cbuffsize);
        ustar_getfile(cblk, scount, cbuff);

        string *strbuff = new_str(64);
        int buffiter=0;
        for(uint32_t i=0; i < csize; i++)
        {
            if(cbuff[i] == '\n')
            {
                if(buffiter == 0)
                    continue;

                if(strncmpi(strbuff->data, "username", 8) == 0)
                {
                    strncpy(strbuff->data+9, username->data, (buffiter-9));
                }
                else if(strncmpi(strbuff->data, "color", 5) == 0)
                {
                    uint8_t col = atoi((strbuff->data+6), 2, 16);
                    ksetcol(col);
                }
                else if(strncmpi(strbuff->data, "homedir", 7) == 0)
                {
                    strncpy((strbuff->data+8), curpath->data, (buffiter-8));
                }
                memset(strbuff->data, 0, buffiter);
                buffiter = 0;
                continue;
            }
            strbuff->data[buffiter++] = cbuff[i];
        }
        del_str(strbuff);
        del_cstr(cbuff, cbuffsize);
        kputs("Loaded config from \"/user.conf\"\n");
    }
}

//
void enter_user_jmp(uint32_t location, int argc, char ** argv, uint32_t stack) {
    __asm__("cli");
    // ToDo
    // set_kernel_stack(current_process->image.stack);

    // Push argc, argv in reverse order (stack)
    PUSH(stack, uint32_t, (uint32_t)argv);
    PUSH(stack, int, argc);

    __asm__ __volatile__(
            "mov %3, %%esp\n"
            "pushl $0xDECADE21\n" // Magic number
            "mov $0x23, %%ax\n" // Segment selector
            "mov %%ax, %%ds\n"
            "mov %%ax, %%es\n"
            "mov %%ax, %%fs\n"
            "mov %%ax, %%gs\n"
            "mov %%esp, %%eax\n" // Move ESP
            "pushl $0x23\n" // Segment selector
            "pushl %%eax\n"
            "pushf\n"
            "popl %%eax\n" // Fix interrupt flag
            "orl  $0x200, %%eax\n"
            "pushl %%eax\n"
            "pushl $0x1B\n"
            "pushl %0\n" // Push entry
            "iret\n"
            : : "m"(location), "r"(argc), "r"(argv), "r"(stack) : "%ax", "%eax"
            );
}
//

void kstart(multiboot_ptr mboot, uint32_t mboot_mag, size_t esp)
{
    (void)mboot;
    (void)mboot_mag;
    (void)esp;

    gdt_init();
    idt_init();
    isr_init();
    irq_init();

    pci_init();

    hdd_sync();

    keybd_init();
    psm_init();

    //
    kclear();
    kputs("Welcome to FloppyOS!\n");
    kputs("If you're stuck, type HELP\n\n");

    bool srboot = false;

    srand(get_timesum());
    
    // "global" vars
    string *curpath = new_str(256);
    curpath->data[0] = '/';

    string *curpass  = new_str(PASSWD_MAX_LEN);
    string *username = new_str(16);
    
    string *cmd = new_str(64);
    //

    load_cfg("/", username, curpath);

    while(1)
    {
        kputstr(username);
        kputs("@fos:");
        kputstr(curpath);
        kputs("> ");
        kgets(cmd, true);
        
        if(strlen(cmd->data) == 0)
            continue;

        if(streqi(cmd, "clear"))
            kclear();
        else if(streqi(cmd, "help"))
        {
            kputs("Commands available:\n"
                  "\tclear            - Clear screen\n"
                  "\tnewfile  [name]  - Create new file\n"
                  "\tls               - Get contents of current folder\n"
                  "\tcd       [path]  - Change current directory\n"
                  "\tsysinfo          - System information\n"
                  "\tusername [name]  - Set username\n"
                  "\tpasswd           - Set password\n"
                  "\tata              - Get storage info\n"
                  "\tcolor    [color] - Set terminal color\n"
                  "\tfree             - Get free RAM amount\n"
                  "\tshutdown         - Shutdown machine\n"
                  "\treboot           - Reboot machine\n"
                  "Commands are case-insensitive\n");
        }
        else if(strneqi(cmd, "sysinfo", 7))
        {
            uint8_t tmp = kgetcol();
            ksetcol(0x0a);
            kputs(KERNEL_NAME " " KERNEL_VER " " KERNEL_ARCH "\n");
            
            kputi((int)cpu_spd());
            kputs("MHz CPU\n");

            uint32_t memsize = (MAX_ADDR-BASE_ADDR);
            uint8_t coef = size_coeff(memsize, 0);
            kputi(memsize >> (10*coef));
            kputc(' ');
            if(coef > 0)
                kputc(coef==SIZE_KB?'K':
                      coef==SIZE_MB?'M':
                      coef==SIZE_GB?'G':
                                    'T');
            kputs("Bytes of RAM available\n");
            
            kputs("Author: Kane Noddic\n");
            kputs("Licence: GNU GPLv3\n");
            ksetcol(tmp);
        }
        else if(strneqi(cmd, "cpuid", 5))
        {
            uint32_t carr[4];
            cpuid(0x00, carr[0], carr[1], carr[3], carr[2]);

            kputs("CPUID: ");
            string *cpustr = new_str(12);

            int p=0;
            for(int i=1; i < 4; i++)
            {
                for(int j=0; j < 4; j++)
                    cpustr->data[p++] = (carr[i] >> (j*8));
            }
            kputs(cpustr->data);
            kputc('\n');

            del_str(cpustr);
        }
        else if(strneqi(cmd, "color", 5))
        {
            int clen = strlen(cmd->data)-6;

            if(clen < 2)
            {
                ksetcol(0x07);
                kputs("Issued color reset\n");
            }  
            else if(clen > 2)
                kputs("Maximum color is FF !\n");
            else
            {
                string *colstr = new_str(clen);
                substr(cmd->data, 6, clen, colstr->data);
                uint8_t newcol = (uint8_t)atoi(colstr->data, 2, 16);
                del_str(colstr);
                if((newcol&0xF) == ((newcol>>4)&0xF))
                    kputs("I am not sure you will be able to read it\n");
                else
                {
                    ksetcol(newcol);
                    kputs("Nice choice\n");
                }
            }
        }
        else if(strneqi(cmd, "exec", 4))
        {
            cstring data = new_cstr(512);

            uint32_t size, blk; // size is number of blocks
            ustar_fetchfile("program", "/", &size, &blk);
            ustar_getfile(blk, size, data);

            uint32_t entry;
            elf_parse(data, 440, &entry);
            if(entry != 0)
            {
                enter_user_jmp(0/* Jump to program? */, 0, NULL, esp);
            }

            del_cstr(data, 512);
        }
        else if(streqi(cmd, "sync"))
        {
            kputs("Syncing disks...");
            hdd_sync();
            kputs("Done\n");
        }
        else if(strneqi(cmd, "memdump", 7))
        {
            int cnt=0;
            for(int i=0; i < 256; i++)
            {
                uint8_t v = ((uint8_t*)BASE_ADDR)[i];
                char * sv = itoa(v, 2, 16, '\0');
                kputs(sv);
                del_cstr(sv, 3); // 2 digits and '\0'
                kputc(' ');
                if(cnt >= 15)
                {
                    cnt = 0;
                    kputc('\n');
                }
                else cnt++;
            }
        }
        else if(streqi(cmd, "ata"))
        {
            hdd_sync();

            ATA_info *hddtableptr = hdd_infostructptr();

            for(int i=0; i < 4; i++)
            {
                if(hddtableptr[i].connected)
                {
                    kputs((i<2)?"Primary ":"Secondary ");
                    kputs((i%2==0)?"storage A: ":"storage B: ");

                    uint8_t adds=0;
                    uint32_t hddsize = (hddtableptr[i].blocks<<9);
                    if(hddsize >= 1024)
                    {
                        adds++;
                        hddsize >>= 10;
                    }
                    kputi(hddsize);
                    kputc(' ');
                    adds += hddtableptr[i].cgrade;
                    if(adds > 0)
                    {
                        kputc(adds==SIZE_KB?'K':
                                  adds==SIZE_MB?'M':
                                  adds==SIZE_GB?'G':
                                                'T');
                        kputc('B');
                    }
                    else
                        kputs("bytes");
                    kputs(hddtableptr[i].type);
                    kputc('\n');
                }
            }
        }
        else if(strneqi(cmd, "newapp", 6))
        {
            cstring zero = new_cstr(512);
            ustar_newfile("program", "/", zero, 440);
            del_cstr(zero, 512);
        }
        else if(strneqi(cmd, "newfile", 7))
        {
#ifndef FOS_READONLY
            int flen = strlen(cmd->data)-8;
            if(flen > 0)
            {
                string *fname = new_str(flen);
                substr(cmd->data, 8, flen, fname->data);
                if(streqi(fname, ".") || streqi(fname, ".."))
                {
                    kputstr(fname);
                    kputs(": is a directory\n");
                    del_str(fname);
                    continue;
                }
                cstring mydata = new_cstr(ATA_BLOCKSIZE);

                kputs("Now write text. Press ESC when you're done\n");

                uint32_t buffsize = ATA_BLOCKSIZE;

                uint32_t i=0;
                while(1)
                {
                    if(i == buffsize)
                    {
                        resize_cstr(mydata, buffsize, (buffsize<<1));
                        buffsize <<= 1; // buffsize *= 2
                    }
                    char ch = kgetc();
                    if(ch == '\b')
                    {
                        if(i >= 1)
                            mydata[--i] = ' ';
                        continue;
                    }
                    if(ch == 0)
                    {
                        i-=1; // don't move
                        continue;
                    }
                    if(ch == 0x03)
                        break;
                    mydata[i++] = ch;
                }

                if(ustar_newfile(fname->data, curpath->data, mydata, i) == 0) // no errors
                {
                    kputs("\nCreated file \"");
                    kputstr(fname);
                    kputs("\"\n");
                }
                else
                {
                    kputs("Failed to write to disk\n");
                }
                del_cstr(mydata, buffsize);
                del_str(fname);
            }
            else
            {
                kputs("Filename can't be empty\n");
            }
#else
            kputs("Running in read-only mode!\n");
#endif
        }
        else if(strneqi(cmd, "rm", 2))
        {
#ifndef FOS_READONLY
            int flen = strlen(cmd->data)-3;
            if(flen > 0)
            {
                string *fname = new_str(flen);
                substr(cmd->data, 3, flen, fname->data);
                if(ustar_rmfile(fname->data, curpath->data))
                {
                    kputstr(fname);
                    kputs(": failed to remove file\n");
                }
                del_str(fname);
            }
            else
            {
                kputs("No file name provided\n");
            }
#else
            kputs("Running in read-only mode!\n");
#endif
        }
        else if(strneqi(cmd, "cd", 2))
        {
            int dlen = strlen(cmd->data)-3;
            if(dlen > 0)
            {
                string *tdir = new_str(dlen);
                substr(cmd->data, 3, dlen, tdir->data);
                int r = ustar_fetchdir(tdir->data, curpath->data);
                if(r == 0)
                {
                    memset(curpath->data, 0, strlen(curpath->data));
                    strcpy(tdir->data, curpath->data);
                }
                else
                {
                    if(r > 1)
                    {
                        kputstr(tdir);
                        kputs(": no such directory\n");
                    }
                }
                del_str(tdir);
            }
            else
            {
                memset(curpath->data, 0, strlen(curpath->data));
                strcpy("/", curpath->data);
            }
        }
        else if(streqi(cmd, "ls"))
        {
            int allocsize = (USTAR_MAX_FPATH_LEN+16) * 16;
            cstring dirs = new_cstr(allocsize); // 16 file infos
            int dircount;

            ustar_listdir(curpath->data, dirs, &dircount);

            int prevdir=0;
            for(int i=0; i < dircount; i++)
            {
                cstring dir = (dirs+prevdir);
                kputs("-> ");
                kputs(dir);
                kputc('\n');

                uint32_t dlen = strlen(dir);
                if(dlen >= MAX_STRLEN)
                {
                    kputs("String length error!");
                    continue;
                }
                prevdir += dlen+1; // string and NUL
            }
            del_cstr(dirs, allocsize);
        }
        else if(strneqi(cmd, "cat", 3))
        {
            int flen = strlen(cmd->data)-4;
            if(flen <= 0)
            {
                kputs("File name can't be empty\n");
                continue;
            }
            string *fname = new_str(flen);
            substr(cmd->data, 4, flen, fname->data);

            uint32_t fsize, fblk;
            int r = ustar_fetchfile(fname->data, curpath->data, &fsize, &fblk);

            if(r == 0)
            {
                uint16_t fsecs = ustar_sectorcount(fsize);
                uint32_t buffsize = ATA_BLOCKSIZE * fsecs;
                cstring buff = new_cstr(buffsize);

                ustar_getfile(fblk, fsecs, buff);

                kputstr(fname);
                kputs(":\n");
                kputsn(buff, fsize);
                kputc('\n');

                del_cstr(buff, buffsize);
            }
            else if(r == 3)
            {
                kputstr(fname);
                kputs(": file not found\n");
            }
            else
                kputs("Error occured\n");
            del_str(fname);
        }
        else if(streqi(cmd, "format"))
        {
#ifndef FOS_READONLY
            ustar_format();
#else
            kputs("Running in read-only mode!\n");
#endif
        }
        else if(streqi(cmd, "pci"))
        {
            dumppci();
        }
        else if(streqi(cmd, "passwd"))
        {
            if(strlen(curpass->data) > 0)
            {
                kputs("Current password: ");
                string *entry = new_str(PASSWD_MAX_LEN);
                kgets(entry, false);
                if(strcmp(entry->data, curpass->data) != 0)
                {
                    del_str(entry);
                    kputs("Password incorrect!\n");
                    continue;
                }
                del_str(entry);
            }
            kputs("Enter new password: ");
            string *newpass = new_str(PASSWD_MAX_LEN);
            kgets(newpass, false);
            strcpy(newpass->data, curpass->data);
            del_str(newpass);
            kputs("Password changed!\n");
        }
        else if(strneqi(cmd, "username", 8))
        {
            int ulen = strlen(cmd->data)-9;
            string *newname = new_str(ulen);
            substr(cmd->data, 9, ulen, newname->data);
            int nl = strlen(newname->data);
            if(nl > 15) // name[15] == '\0'
            {
                kputs("Username length must be at most 15 chars\n");
            }
            else if(nl <= 0)
            {
                kputs("Username can't be empty\n");
            }
            else
            {
                strcpy(newname->data, username->data);
            }
            del_str(newname);
        }
        else if(streqi(cmd, "shutdown"))
        {
            kputs("Now PC can be turned off\nGoodbye! :)\n");
            shutdown();
            break;
        }
        else if(streqi(cmd, "reboot"))
        {
            srboot = true;
            break;
        }
        else if(streqi(cmd, "time"))
        {
            cstring timestr = get_time_cstr();

            kputs("Time: ");
            kputs(timestr);
            kputc('\n');

            del_cstr(timestr, 9);
        }
        else if(streqi(cmd, "memload"))
        {
            uint32_t ram_used = memload();
            char coeff = '\0';
            int left = 0;
            if(ram_used >= 1024)
            {
                left += ram_used - ((ram_used/1024) * 1024);
                ram_used /= 1024;
                if(ram_used >= 1024)
                {
                    ram_used /= 1024;
                    left += ram_used - ((ram_used/1024) * 1024);
                    coeff = 'M';
                }
                else
                    coeff = 'K';
            }
            kputi((int)ram_used);
            kputc(',');
            kputi(left);
            kputc(' ');
            kputc(coeff);
            kputs("Bytes used\n");
        }
        else if(streqi(cmd, "slots"))
        {
            kputc('\n');
            
            char randv, charv;
            
            for(int i=0; i < 3; i++)
            {
                randv = rand()%100;
                charv = (randv/30)%4+3;
                uint8_t col = 0xf0;
                
                if(charv == 3)
                    col = 0xfc;
                else if(charv == 4)
                    col = 0xf9;
                else if(charv == 5)
                    col = 0xf2;
                kputcc(charv, col);
            }
            
            kputc('\n');
        }
        else if(streqi(cmd, "hello"))
            kputs("Hello!\n");
        else
        {
            kputstr(cmd);
            kputs(": command not found!\n");
        }
    }
    if(srboot)
        reboot();
}
