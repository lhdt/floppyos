#include "gui/hcore.h"
#include "drivers/vga.h"

void hw_set_size(void *widget, uint16_t w, uint16_t h)
{
    if(widget == NULL)
        return;

    uint16_t *wptr = (uint16_t*)widget;
    wptr[2] = w;
    wptr[3] = h;
}

void hw_set_pos(void *widget, uint16_t x, uint16_t y)
{
    if(widget == NULL)
        return;

    uint16_t *wptr = (uint16_t*)widget;
    wptr[0] = x;
    wptr[1] = y;
}

void *hw_new_widget(uint8_t wtype)
{
    if(wtype == HW_WINDOW)
    {
        hWindow window = (hWindow)new_cstr(sizeof(struct hWindow_s));
        window->wid = HW_WINDOW;
        return window;
    }
    return NULL;
}

void hw_del_widget(void *widget)
{
    if(widget == NULL)
        return;

    uint8_t *wptr = (uint8_t*)widget;
    if(wptr[8] == HW_WINDOW)
        del_cstr((cstring)widget, sizeof(struct hWindow_s));
}

void hw_draw_widget(void *widget)
{
    if(widget == NULL)
        return;

    uint8_t *wptr = (uint8_t*)widget;

    if(wptr[8] == HW_WINDOW)
    {
        hWindow win_ptr = (hWindow)widget;

        // window bar
        vga_fillrect(win_ptr->x, win_ptr->y, win_ptr->w, 8, win_ptr->aCol);
        vga_fillrect(win_ptr->x+win_ptr->w-8, win_ptr->y, 8, 8, VGA_COLOR_RED);
        // window body
        vga_fillrect(win_ptr->x, win_ptr->y+8, win_ptr->w, win_ptr->h-8, win_ptr->mCol);
    }
}

void hw_drawbuff_init(uint16_t w, uint16_t h)
{
    (void)w;
    (void)h;
}
