#include "elf.h"
#include "stdlib.h"
#include "memory.h"

extern void kputh(uint32_t);
extern void kputs(cstring);

void elf_parse(const void *data, size_t size, uint32_t *entry)
{
    *entry = 0;

    if(size <= sizeof(elf32_header_t))
    {
        return;
    }

    uint32_t sign = (*(uint32_t*)data);
    if(sign != ELF_SIGN)
        return;

    const elf32_header_t *hptr = data;

    *entry = hptr->e_entry;
}
