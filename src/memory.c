#include "include/memory.h"

extern int strlen(const cstring);

static char *lastaddr = (char*)BASE_ADDR;

cstring new_cstr(int len)
{
    if(!VALIDADDR((uint32_t)(lastaddr+len)))
        return NULL;
    for(int i=0; i < len; lastaddr[i++]=0); // clear
    lastaddr += len;
    return (lastaddr-len);
}

void resize_cstr(cstring s, int o_len, int n_len)
{
    if(!VALIDADDR((uint32_t)(lastaddr-o_len)+n_len))
        return;

    if(lastaddr - o_len == s)
        lastaddr += (n_len - o_len);
}

void del_cstr(cstring s, int len)
{
    if(!VALIDADDR((uint32_t)s))
        return;

    for(int i=0; i < len; i++)
        s[i] = 0;
    if(lastaddr-len == s)
        lastaddr -= len;
    s = 0;
}

uint32_t memload()
{
    return ((uint32_t)lastaddr - BASE_ADDR);
}

void memset(generic p, char c, int size)
{
    if(!VALIDADDR((uint32_t)p))
        return;
    for(int i=0; i < size; i++)
        ((char*)p)[i] = c;
}

cstring *new_cstr_array(int s_len, int a_len)
{
    if(!VALIDADDR((uint32_t)(lastaddr+(s_len*a_len))))
        return NULL;
    return NULL;
}

void del_cstr_array(cstring *sa, int s_len, int a_len)
{
    if(!VALIDADDR((uint32_t)sa))
        return;
}

char *last_ptr()
{
    return lastaddr;
}

void memcpy(const generic src, generic dst, int size)
{
    for(int i=0; i < size; i++)
        ((char*)dst)[i] = ((char*)src)[i];
}

string *new_str(int len)
{
    string *s = (string*)new_cstr(sizeof(string));

    s->size = len;
    s->data = new_cstr(len+1);
    return s;
}

void del_str(string *s)
{
    if(!VALIDADDR((uint32_t)s))
        return;

    del_cstr(s->data, s->size+1);
    if((size_t)(lastaddr-sizeof(string)) == (size_t)s)
    {
        lastaddr -= sizeof(string);
        s = NULL;
    }
}

string *create_str(const cstring src)
{
    int len = strlen(src);
    string *s = new_str(len);
    memcpy(src, s->data, (len+1)); // str+'\0'
    return s;
}
