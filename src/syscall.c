#include "syscall.h"
#include "drivers/io.h"

void syscall(struct regs_s *r)
{
    uint32_t eax, ebx, edi, esi;
    __asm__ __volatile__("mov %%eax, %0" : "=a"(eax));
    __asm__ __volatile__("mov %%ebx, %0" : "=a"(ebx));
    __asm__ __volatile__("mov %%edi, %0" : "=a"(edi));
    __asm__ __volatile__("mov %%esi, %0" : "=a"(esi));
    //
    if(eax == CALL_PUTC)
        kputc(ebx);
    else if(eax == CALL_PUTI)
        kputi(ebx);
    else if(eax == CALL_PUTH)
        kputi(ebx);
    else if(eax == CALL_PUTS)
        kputsn((char*)esi, edi);
}

void syscall_init()
{
    isr_install_handler(0x7F, syscall);
}
